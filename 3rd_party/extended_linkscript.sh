#!/bin/bash

# This script can be used by a developer who wants to check his changes to the Joomleague sources on a Joomla site.
#
# Prerequisites:
# - Joomla site is installed
# - The Joomleague component is installed in the standard Joomla way.
# - The git repository is available on your developer machine.
# 
# Actions by the script:
# - replaces all Joomleague files and directories in the Joomla installation by softlinks to their version in the git repository.
# - for both frontend and backend
# - for both the component and all installed modules
# - for all installed languages that have been installed on the site

echo "###################################################################################################"
echo "### Script that replaces Joomleague in a Joomla installation by softlinks to the developer area ###"
echo "###################################################################################################"

if [[ $# == 2 ]]
then
  # readlink translates relative to absolute paths
  WWW=`readlink -f $1`
  GIT=`readlink -f $2`
else
  echo "USAGE: $0 <joomla-root> <git-root>"
  echo "<joomla-root> = location of the joomla installation for which to replace Joomleague by links to GIT"
  echo "                e.g. ~/public_html/somesite"
  echo "<git-root>    = location of the root of the GIT work area"
  echo "                e.g. ~/Projects/joomleague"
  exit 1
fi

if [[ ! -d $WWW/components/com_joomleague ]]
then
  echo "The specified joomla root directory does not contain directory components/com_joomleague."
  echo "Joomleague must already have been installed on this Joomla site."
  echo "Please check if you supplied the correct directory."
  echo "Exiting the script without having made any changes."
  exit 1
fi

if [[ ! -d $GIT/com_joomleague/components/com_joomleague ]]
then
  echo "The specified git root directory does not contain directory com_joomleague/components/com_joomleague."
  echo "Please check if you supplied the correct directory."
  echo "Exiting the script without having made any changes."
  exit 1
fi

GIT=$GIT/com_joomleague

# Find the installed languages for the Joomla installation
pushd $WWW/administrator/language
languages=$(find . -maxdepth 1 -type d -iname '[a-z][a-z]-[A-Z][A-Z]' | sed s'/\.\///')
popd

# Replace administrator component by a link to its content in the GIT work area      
joomleague_admin_component="administrator/components/com_joomleague"
echo Replace joomleague administrator component by a link to its GIT work area location
rm -rf $WWW/$joomleague_admin_component
ln -s $GIT/$joomleague_admin_component $WWW/$joomleague_admin_component

# Link the administrator language files to the respective locations in the GIT work area
for language in $languages
do
  for langfile in com_joomleague.ini com_joomleague.sys.ini com_joomleague_countries.ini com_joomleague_sport_types.ini
  do
    admin_component_language=$language.$langfile
    echo Replace $admin_component_language by a link to its GIT work area location
    rm -f $WWW/administrator/language/$language/$admin_component_language
    ln -s $GIT/$joomleague_admin_component/language/$language/$admin_component_language $WWW/administrator/language/$language/
  done
done

# Replace the administrator modules on the website by links to their contents in the GIT work area
# TODO: administrator/modules/mod_title & mod_toolbar
for module in mod_joomleague_adminpanel_icon 
do
  echo Replace \"$module\" by a link to its GIT work area location
  rm -rf $WWW/administrator/modules/$module
  ln -s $GIT/$joomleague_admin_component/modules/$module $WWW/administrator/modules/

  for language in $languages
  do
    for file_ext in ini sys.ini
    do
      module_language=$language.$module.$file_ext
      echo Replace $module_language by a link to its GIT work area location
      rm -f $WWW/administrator/language/$language/$module_language
      ln -s $GIT/$joomleague_admin_component/modules/$module/language/$language/$module_language $WWW/administrator/language/$language/
    done
  done
done

# Find the installed languages for the Joomla installation
pushd $WWW/language
languages=$(find . -maxdepth 1 -type d -iname '[a-z][a-z]-[A-Z][A-Z]' | sed s'/\.\///')
popd

# Replace component by a link to its content in the GIT work area
joomleague_component="components/com_joomleague"
echo Replace joomleague component by a link to its GIT work area location
rm -rf $WWW/$joomleague_component
ln -s $GIT/$joomleague_component $WWW/$joomleague_component

# Link the language files to the respective locations in the GIT work area
for language in $languages
do
  component_language=$language.com_joomleague.ini
  echo Replace $component_language by a link to its GIT work area location
  rm -f $WWW/language/$language/$component_language
  ln -s $GIT/$joomleague_component/language/$language/$component_language $WWW/language/$language/
done

# Replace the modules on the website by links to their contents in the GIT work area
pushd $WWW/modules
for module in $(find . -maxdepth 1 -type d -iname 'mod_joomleague_*' | sed s'/\.\///')
do
  # Only replace modules that are really installed on the Joomla installation
  if [[ -d $WWW/modules/$module ]]
  then
    echo Replace \"$module\" by a link to its GIT work area location
    rm -rf $WWW/modules/$module
    ln -s $GIT/$joomleague_component/modules/$module $WWW/modules/

    for language in $languages
    do
      for file_ext in ini sys.ini
      do
        module_language=$language.$module.$file_ext
        echo Replace $module_language by a link to its GIT work area location
        rm -f $WWW/language/$language/$module_language
        ln -s $GIT/$joomleague_component/modules/$module/language/$language/$module_language $WWW/language/$language/
      done
    done
  fi
done
popd

# Replace the standard media for Joomleague by a link to the media folder in the GIT work area
echo Replace joomleague media by a link to its GIT work area location
rm -rf $WWW/media/com_joomleague
ln -s $GIT/media/com_joomleague $WWW/media/
