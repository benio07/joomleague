# For example
# ./3rd_party/merge_request.sh 195 master

# Check out a new branch for integration
git checkout -b merge-requests/$1

# Fetch the merge request into this branch
git pull \
  git://gitorious.org/joomleague/joomleague.git \
  refs/merge-requests/$1

# Show the commits, assess they are okay
# git log --pretty=oneline --abbrev-commit master..merge-requests/$1

# To apply the changes to your branch:
git checkout $2
git merge merge-requests/$1