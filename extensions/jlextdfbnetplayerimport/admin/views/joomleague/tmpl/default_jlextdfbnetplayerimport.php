<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
//this is an example menu for an extension

	$imagePath='administrator/components/com_joomleague/assets/images/';
	// active pane selector
	switch (JRequest::getVar('view'))
	{
		case 'jlextdfbnetplayerimport':	$active=count($this->tabs);
		break;
		default: $active=count($this->tabs);
	}
	
	$pane=new stdClass();
	$pane->id = 'ExtensionDFBNETPLAYERIMPORT';
	$pane->title=JText::_('COM_JOOMLEAGUE_T_MENU_DFB_NET_IMPORT');
	$pane->name='ExtMenuExtensionDFBNETPLAYERLIMPORT';
	$pane->alert=false;
	$tabs[]=$pane;
	
	$link5=array();
	$label5=array();
	$limage5=array();
	$link5[]=JRoute::_('index.php?option=com_joomleague&view=jlextdfbnetplayerimport&active='.$active);
	$label5[]=JText::_('COM_JOOMLEAGUE_T_MENU_DFB_NET_IMPORT');
	$limage5[]=JHtml::image($imagePath.'icon-16-FrontendSettings.png',JText::_('COM_JOOMLEAGUE_T_MENU_DFB_NET_IMPORT'));
	
	$link[]=$link5;
	$label[]=$label5;
	$limage[]=$limage5;
	
	
	$n=0;
	
	$selector = "joomleagueaccordiondfbmenu";
	echo JHtml::_('bootstrap.startAccordion', $selector, array(	'active'=>$this->active,
																'allowAllClose' => true,
																'startTransition' => true));
												
	foreach ($tabs as $tab)
	{
		$title=JText::_($tab->title);
		echo JHtml::_('bootstrap.addSlide', $selector, $title, $tab->name);
		?>
		<div style="float: left;">
			<table><?php
				for ($i=0;$i < count($link[$n]); $i++)
				{
					?><tr><td><b><a href="<?php echo $link[$n][$i]; ?>" title="<?php echo JText::_($label[$n][$i]); ?>">
							<?php echo $limage[$n][$i].' '.JText::_($label[$n][$i]); ?>
					</a></b></td></tr><?php
				}
				?></table>
		</div>
		<?php
		echo JHtml::_('bootstrap.endSlide');
		$n++;
	}
	echo JHtml::_('bootstrap.endAccordion');
?>
