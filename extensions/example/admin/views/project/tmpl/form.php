<?php defined('_JEXEC') or die('Restricted access');

?>
<form action="index.php" method="post" id="adminForm">
	<div class="col50" id="project">
	<?php
	$selector = 'project';
	echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'panel1')); 
		
	echo JHtml::_('bootstrap.addTab', $selector, 'panel1', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
	echo $this->loadTemplate('details');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'panel2', JText::_('COM_JOOMLEAGUE_TABS_DATE'));
	echo $this->loadTemplate('date');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'panel3', JText::_('COM_JOOMLEAGUE_TABS_PROJECT'));
	echo $this->loadTemplate('project');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'panel4', JText::_('COM_JOOMLEAGUE_TABS_COMPETITION'));
	echo $this->loadTemplate('competition');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'panel5', JText::_('COM_JOOMLEAGUE_TABS_FAVORITE'));
	echo $this->loadTemplate('favorite');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'panel6', JText::_('COM_JOOMLEAGUE_TABS_PICTURE'));
	echo $this->loadTemplate('picture');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'panel7', JText::_('COM_JOOMLEAGUE_TABS_EXTENDED'));
	echo $this->loadTemplate('extended');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.endTabSet');
	?>
	<div class="clr"></div>
	<input type="hidden" name="option" value="com_joomleague" /> 
	<input type="hidden" name="task" value="" /> 
	<input type="hidden"name="oldseason" value="<?php echo $this->project->season_id; ?>" />
	<input type="hidden" name="oldleague" value="<?php echo $this->project->league_id; ?>" /> 
	<input type="hidden" name="cid[]" value="<?php echo $this->project->id; ?>" />
	<?php echo JHtml::_('form.token')."\n"; ?>
	</div>
</form>
