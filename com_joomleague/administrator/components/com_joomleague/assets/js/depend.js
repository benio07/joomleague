/**
 * @copyright	Copyright (C) 2005-2015 joomleague.at. All rights reserved.
 * @license	GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

/**
 * javascript for dependant element xml parameter
 * 
 * 
 */


// The new Joomla3 backend template (isis) works with jQuery Chosen, which
// complicates matters with handling of the selected menu parameters. The
// old-fashioned template (hathor) just uses <select> elements.
// For the new backend template some extra work must be done.
var formIsUsingjQueryChosen;
	
// add update of field when fields it depends on change.
window.addEvent('domready', function() {
	$$('.mdepend').addEvent('click', function() {
		// rebuild hidden field list
		var sel = new Array();
		var i = 0;
		this.getElements('option').each(function(el) {
			if (el.getProperty('selected')) {
				sel[i++] = el.value;
			}
		});
		this.getParent().getElement('input').value = sel.join("|");
	});

	$$('.depend').each(function(element) {
		var depends = element.getProperty('depends');
		var myelement = element;
		var prefix = getElementIdPrefix(element);
		if (formIsUsingjQueryChosen === undefined) {
			formIsUsingjQueryChosen = (element.nextElementSibling != null && element.nextElementSibling.getElement('a.chzn-single') != null);
		}
		
		// Attach a click event to the anchor that opens the select box,
		// so the ul.chzn-results is filled when "opening" the select box.
		if (formIsUsingjQueryChosen) {
			var anchor = element.nextElementSibling.getElement('a.chzn-single')
			$(anchor).addEvent('click', function() {
				var combo = this.parentElement.previousElementSibling;
				fillDropResults(combo);
			})
		}

		// Attach update_depend to the change event of all elements it depends upon,
		// so that when (one of) the dependencies change, the element is refreshed. 
		depends.split(',').each(function(el) {
			if (formIsUsingjQueryChosen) {
				// Unfortunately the <select> is not updated by Joomla when changing the selection
				// of an item. An alternative would be to monitor for changes on the <span>, but
				// that is not supported. Another possibility is to watch for click event on the
				// <li>, but this has an extra complication: there are no <li>'s under the <ul class="chzn-results">
				// before clicking on the <a class="chzn-single">.
				$(prefix + el).nextElementSibling.getElement('a.chzn-single').addEvent('click', function(event) {
					this.parentElement.getElements('ul.chzn-results li').addEvent('click', function(event) {
						update_depend(myelement);
					});
				});
			} else {
				$(prefix + el).addEvent('change', function(event) {
					update_depend(myelement);
			});
			}
		});

		// Refresh the element also after the page is loaded (to fill the element)
		update_depend(myelement);
	});
});

// update dependant element function
function update_depend(element) {
	var combo = element;
	var anchorSpan = formIsUsingjQueryChosen ? combo.nextElementSibling.getElement('a.chzn-single span') : undefined;
	var prefix = getElementIdPrefix(element);
	var required = element.getProperty('required') != null ? "&required=true" : "&required=false" ;
	var selectedItems = combo.getProperty('current').split('|');
	var depends = combo.getProperty('depends').split(',');
	var dependquery = '';
	depends.each(function(str) {
		dependquery += '&' + str + '=' + $(prefix + str).value;
	});

	var task = combo.getProperty('task');
	var postStr = '';
	var url = 'index.php?option=com_joomleague&format=json&task=ajax.' + task 
			+ required + dependquery;
	var theAjax = new Request.JSON({
		url : url,
		method : 'post',
		postBody : postStr,
		onSuccess : function(response) {
			var options = response;
			var headingLine = null;
			combo.empty();
			options.each(function(el) {
				if (typeof el == "undefined") return;
				if (selectedItems != null && selectedItems.indexOf(el.value) != -1) {
					el.selected = "selected";
					if (formIsUsingjQueryChosen) {
						anchorSpan.innerHTML = el.text;
						//anchorSpan.removeClass('chzn-default');
					}
				}
				new Element('option', el).inject(combo);
			});
			if (formIsUsingjQueryChosen) {
				if (options.length == 0) {
					anchorSpan.innerHTML = "";
				} else {
					var existingValue = false;
					for (var i = 0; i < options.length; i++) {
						if (options[i].text === anchorSpan.innerHTML) {
							existingValue = true;
						}
					}
					if (!existingValue) {
						anchorSpan.innerHTML = options[0].text;
						options[0].selected = "selected"
						//anchorSpan.removeClass('chzn-default'); door te selecten
					}
					fillDropResults(combo);
				}
			}
			// Make sure to inform others who are dependent on us, so they also update
			combo.fireEvent('change');
			// For mdepend make sure that the hidden input is updated
			combo.fireEvent('click');
		}
	});

	theAjax.post();
}

function fillDropResults(combo) {
	var unorderedList = combo.nextElementSibling.getElement("ul.chzn-results");
	unorderedList.empty();
	for (var i = 0; i < combo.length; i++) {
		var li = new Element('li', {html: combo.options[i].text});
		li.addClass('active-result');
		if (combo.options[i].selected) {
			li.addClass("result-selected highlighted");
		}
		li.setProperty('data-option-array-index', i);
		li.inject(unorderedList)
	}
}

/** The element IDs can be either "jform_request_" (for menu items) or "jform_params_" (for modules)
 *  This function will check if we have to do with menu items or modules, and return the right
 *  prefix to be used for element-IDs */ 
function getElementIdPrefix(el) {
	var id = el.getProperty('id');
	var infix = id.replace(/^jform_(\w+)_.*$/, "$1");
	return infix.match("request") ? "jform_request_" : "jform_params_";
}