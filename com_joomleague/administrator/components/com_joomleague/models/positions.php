<?php
/**
 * @copyright	Copyright (C) 2006-2015 joomleague.at. All rights reserved.
 * @license		GNU/GPL,see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License,and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
require_once JPATH_COMPONENT.'/models/list.php';

/**
 * Joomleague Component Teams Model
 */
class JoomleagueModelPositions extends JoomleagueModelList
{
	var $_identifier = "positions";
	
	function _buildQuery()
	{
		// Get the WHERE and ORDER BY clauses for the query
		$where = $this->_buildContentWhere();
		$orderby=$this->_buildContentOrderBy();
		$query='	SELECT	a.*,
							pop.name AS parent_name,
							st.name AS sportstype,
							u.name AS editor,

							(select count(*) FROM #__joomleague_position_eventtype
							WHERE position_id=a.id) countEvents,
							(select count(*) FROM #__joomleague_position_statistic
							WHERE position_id=a.id) countStats

					FROM	#__joomleague_position AS a
					LEFT JOIN #__joomleague_sports_type AS st ON st.id=a.sports_type_id
					LEFT JOIN #__joomleague_position AS pop ON pop.id=a.parent_id
					LEFT JOIN #__users AS u ON u.id=a.checked_out ' .
		$where.$orderby;
		return $query;
	}

	function _buildContentOrderBy()
	{
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$filter_order		= $app->getUserStateFromRequest($option.'po_filter_order',		'filter_order',		'a.ordering',	'cmd');
		$filter_order_Dir	= $app->getUserStateFromRequest($option.'po_filter_order_Dir',	'filter_order_Dir',	'',				'word');
		if ($filter_order == 'a.ordering')
		{
			$orderby=' ORDER BY a.parent_id ASC,a.ordering '.$filter_order_Dir;
		}
		else
		{
			$orderby=' ORDER BY '. $filter_order.' '.$filter_order_Dir;
		}
		return $orderby;
	}

	function _buildContentWhere()
	{
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$filter_sports_type	= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_sports_type',	'filter_sports_type','',			'int');
		$filter_state		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_state',		'filter_state',		'',				'word');
		$filter_order		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_order',		'filter_order',		'a.ordering',	'cmd');
		$filter_order_Dir	= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_order_Dir',	'filter_order_Dir',	'',				'word');
		$search				= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.search',				'search',			'',				'string');
		$search_mode		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.search_mode',		'search_mode',		'',				'string');
		$search=JString::strtolower($search);
		$where=array();
		if ($filter_sports_type> 0)
		{
			$where[]='a.sports_type_id='.$this->_db->Quote($filter_sports_type);
		}
		if ($search)
		{
			if ($search_mode)
			{
				$where[]='LOWER(a.name) LIKE '.$this->_db->Quote($search.'%');
			}
			else
			{
				$where[]='LOWER(a.name) LIKE '.$this->_db->Quote('%'.$search.'%');
			}
		}
		if ($filter_state)
		{
			if ($filter_state == 'P')
			{
				$where[]='a.published=1';
			}
			elseif ($filter_state == 'U')
			{
				$where[]='a.published=0';
			}
		}
		$where=(count($where) ? ' WHERE '.implode(' AND ',$where) : '');
		return $where;
	}

	/**
	 * Method to return the positions array (id,name) 
	 *
	 * @access	public
	 * @return	array
	 */
	function getParentsPositions()
	{
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$project_id=$app->getUserState($option.'project');
		//get positions already in project for parents list
		//support only 2 sublevel, so parent must not have parents themselves
		$query='	SELECT	a.id AS value,
							a.name AS text
					FROM #__joomleague_position AS a
					WHERE a.parent_id=0
					ORDER BY a.ordering ASC 
					';
		$this->_db->setQuery($query);
		if (!$result=$this->_db->loadObjectList())
		{
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		return $result;
	}

}
?>