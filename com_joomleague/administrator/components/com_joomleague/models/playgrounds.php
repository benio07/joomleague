<?php
/**
 * @copyright	Copyright (C) 2006-2015 joomleague.at. All rights reserved.
 * @license		GNU/GPL,see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License,and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
require_once JPATH_COMPONENT.'/models/list.php';

/**
 * Joomleague Component Venues Model
 */
class JoomleagueModelPlaygrounds extends JoomleagueModelList
{
	var $_identifier = "playgrounds";
	
	function _buildQuery()
	{
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select($this->getState('item.select','a.*'));
		$query->from('#__joomleague_playground AS a');
		
		// Club
		$query->select('c.name AS club');
		$query->join('LEFT', '#__joomleague_club AS c ON c.id = a.club_id');
		
		// User
		$query->select('u.name AS editor');
		$query->join('LEFT', '#__users AS u ON u.id = a.checked_out');
		
		
		// WHERE
		$filter_order		= $app->getUserStateFromRequest($option.'v_filter_order',		'filter_order',		'a.ordering',	'cmd');
		$filter_order_Dir	= $app->getUserStateFromRequest($option.'v_filter_order_Dir',	'filter_order_Dir',	'',				'word');
		$search				= $app->getUserStateFromRequest($option.'v_search',			'search',			'',				'string');
		$search_mode		= $app->getUserStateFromRequest($option.'v_search_mode',		'search_mode',		'',				'string');
		$search=JString::strtolower($search);
		
		
		// WHERE - SEARCH
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = '.(int) substr($search, 3));
			} else {
				if ($search_mode) {
					$search = $db->Quote($db->escape($search, true).'%');
				} else {
					$search = $db->Quote('%'.$db->escape($search, true).'%');
				}
				
				if($search) {
					$query->where('(LOWER(a.name) LIKE '.$search.')');
				}
			}
		}
		
		// ORDERBY
		if ($filter_order == 'a.ordering')
		{
			$query->order(array($db->escape('a.ordering '.$filter_order_Dir)));
		}
		else
		{
			$query->order(array($db->escape($filter_order.' '.$filter_order_Dir),'a.ordering'));
		}
		
		return $query;
	}
}
