<?php
/**
 * @copyright	Copyright (C) 2006-2015 joomleague.at. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
require_once JPATH_COMPONENT.'/models/list.php';

/**
 * Joomleague	Component Projects Model
 * @package		JoomLeague
 */
class JoomleagueModelProjects extends JoomleagueModelList
{
	public $_identifier = "projects";
	
	function _buildQuery()
	{
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select($this->getState('item.select','a.*'));
		$query->from('#__joomleague_project AS a');
		
		// Sportstype
		$query->select('st.name AS sportstype');
		$query->join('LEFT', '#__joomleague_sports_type AS st ON st.id = a.sports_type_id');
		
		// Season
		$query->select('s.name AS season');
		$query->join('LEFT', '#__joomleague_season AS s ON s.id = a.season_id');
		
		// League
		$query->select('l.name AS league');
		$query->join('LEFT', '#__joomleague_league AS l ON l.id = a.league_id');
		
		// User
		$query->select('u.name AS editor');
		$query->join('LEFT', '#__users AS u ON u.id = a.checked_out');
		
		// WHERE
		$filter_league		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_league',		'filter_league','',			'int');
		$filter_sports_type	= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_sports_type',	'filter_sports_type','',	'int');
		$filter_season		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_season',		'filter_season','',			'int');
		$filter_state		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_state',		'filter_state',		'P',	'word');
		$search				= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.search',				'search',			'',		'string');
		$search_mode		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.search_mode',		'search_mode',		'',		'string');
		$search=JString::strtolower($search);
		
		if($filter_league > 0) {
			$query->where(array('a.league_id = ' . $db->quote($filter_league)));
		}
		if($filter_season > 0) {
			$query->where(array('a.season_id = ' . $db->quote($filter_season)));
		}
		if($filter_sports_type > 0) {
			$query->where(array('a.sports_type_id = ' . $db->quote($filter_sports_type)));	
		}
		
		// WHERE - PUBLISHED
		if ($filter_state)
		{
			if ($filter_state == 'P')
			{
				$query->where('a.published = 1');
			}
			elseif ($filter_state == 'U' )
			{
				$query->where('a.published = 0');
			}
			elseif ($filter_state == 'A' )
			{
				$query->where('a.published = 2');
			}
			elseif ($filter_state == 'T' )
			{
				$query->where('a.published = -2');
			}
		}
		
		
		// WHERE - SEARCH
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
		
				if($search) {
					$query->where('(LOWER(a.name) LIKE '.$search.')');
				}
			}
		}
		
		// ORDERBY
		$filter_order		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_order',		'filter_order',		'a.ordering',	'cmd');
		$filter_order_Dir	= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_order_Dir',	'filter_order_Dir',	'',				'word');
		
		if ($filter_order == 'a.ordering')
		{
			$query->order(array($db->escape('a.ordering '.$filter_order_Dir)));
		}
		else
		{
			$query->order(array($db->escape($filter_order.' '.$filter_order_Dir),'a.ordering'));
		}
		
		return $query;
	}


	/**
	* Method to check if the project to be copied already exists
	*
	* @access  public
	* @return  array
	*/
	function cpCheckPExists( $post )
	{
		$name = 		$post['name'];
		$league_id = 	$post['league_id'];
		$season_id = 	$post['season_id'];
		$old_id = 		$post['old_id'];

		//check project unicity if season and league are not both new
		if ( $league_id && $season_id )
		{
			$query = '	SELECT id FROM #__joomleague_project
						WHERE name = ' . $this->_db->Quote($name) . '
						AND league_id = ' . $league_id . '
						AND season_id = ' . $season_id;

			$this->_db->setQuery( $query );
			$this->_db->query();
			$num = $this->_db->getAffectedRows();

			if ( $num > 0 )
			{
				return false;
			}
		}

		return true;
	}

	/**
	* Method to assign teams of an existing project to a copied project
	*
	* @access	public
	* @return	array
	*/
	function cpCopyStaff( $post )
	{
		$old_id = (int)$post['old_id'];
		$project_id = (int)$post['id'];

		$query = '	SELECT	ts.projectteam_id,
							ts.person_id,
							ts.project_position_id,
							jt.id,
							jt.team_id
					FROM #__joomleague_team_staff ts
					LEFT JOIN #__joomleague_project_team as jt ON jt.id = ts.projectteam_id
					WHERE jt.project_id = ' . $old_id . '
					ORDER BY jt.id ';

		$this->_db->setQuery( $query );

		if ( $results = $this->_db->loadAssocList() )
		{
			foreach( $results as $result )
			{
				$query = '	SELECT	jt.id,
									jt.team_id
							FROM #__joomleague_project_team jt
							WHERE jt.project_id = ' . $project_id . ' AND jt.team_id = ' . $result['team_id'] . '
							ORDER BY jt.id ';

				$this->_db->setQuery( $query );
				$newprojectteam_id = $this->_db->loadResult();

				$p_staff =& $this->getTable();
				$p_staff->bind( $result );
				$p_staff->set( 'teamstaff_id', NULL );
				$p_staff->set( 'projectteam_id', $newprojectteam_id );

				if ( !$p_staff->store() )
				{
					echo $this->_db->getErrorMsg();
					return false;
				}
			}
		}
		return true;
	}

	/**
	* Method to return a season array (id, name)
	*
	* @access	public
	* @return	array seasons
	*/
	function getSeasons()
	{
		$query = '	SELECT	id,
							name
					FROM #__joomleague_season
					ORDER BY name ASC ';

		$this->_db->setQuery( $query );

		if ( !$result = $this->_db->loadObjectList() )
		{
			$this->setError( $this->_db->getErrorMsg() );
			return false;
		}

		return $result;
	}

}
?>