<?php
/**
 * @copyright	Copyright (C) 2006-2015 joomleague.at. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.model' );
require_once JPATH_COMPONENT.'/models/list.php';

/**
 * Joomleague Component Events Model
 */
class JoomleagueModelStatistics extends JoomleagueModelList
{
	var $_identifier = "statistics";
	
	function _buildQuery()
	{
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select($this->getState('item.select','a.*'));
		$query->from('#__joomleague_statistic AS a');
		
		// Sportstype
		$query->select('st.name AS sportstype');
		$query->join('LEFT', '#__joomleague_sports_type AS st ON st.id = a.sports_type_id');
		
		// User
		$query->select('u.name AS editor');
		$query->join('LEFT', '#__users AS u ON u.id = a.checked_out');
		
		// WHERE
		$filter_sports_type	= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_sports_type',	'filter_sports_type','',	'int');
		$filter_state		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_state',		'filter_state',		'',				'word');
		$search				= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.search',				'search',			'',				'string');
		$search_mode		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.search_mode',		'search_mode',		'',				'string');
		$search=JString::strtolower($search);
		
		// WHERE - PUBLISHED
		if ($filter_state)
		{
			if ($filter_state == 'P')
			{
				$query->where('a.published = 1');
			}
			elseif ($filter_state == 'U' )
			{
				$query->where('a.published = 0');
			}
			elseif ($filter_state == 'A' )
			{
				$query->where('a.published = 2');
			}
			elseif ($filter_state == 'T' )
			{
				$query->where('a.published = -2');
			}
		}
		
		// WHERE - SPORTSTYPE
		if($filter_sports_type > 0) {
			$query->where(array('a.sports_type_id = ' . $db->quote($filter_sports_type)));
		}
		
		// WHERE - SEARCH
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = '.(int) substr($search, 3));
			} else {
				$search = $db->Quote('%'.$db->escape($search, true).'%');
		
				if($search) {
					$query->where('(LOWER(a.name) LIKE '.$search.')');
				}
			}
		}
		
		// ORDERBY
		$filter_order		= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_order',		'filter_order',		'a.ordering',	'cmd');
		$filter_order_Dir	= $app->getUserStateFromRequest($option.'.'.$this->_identifier.'.filter_order_Dir',	'filter_order_Dir',	'',				'word');
		if ($filter_order == 'a.ordering')
		{
			$query->order(array($db->escape('a.ordering '.$filter_order_Dir)));
		}
		else
		{
			$query->order(array($db->escape($filter_order.' '.$filter_order_Dir),'a.ordering'));
		}
		
	
		return $query;
	}
}
?>