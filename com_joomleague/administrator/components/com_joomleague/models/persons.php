<?php
/**
 * @copyright	Copyright (C) 2006-2015 joomleague.at. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
require_once JPATH_COMPONENT.'/models/list.php';

/**
 * Joomleague Component Persons Model
 */
class JoomleagueModelPersons extends JoomleagueModelList
{
	var $_identifier = "persons";

	function _buildQuery()
	{
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select($this->getState('item.select','a.*'));
		$query->from('#__joomleague_person AS a');
		
		// User
		$query->select('u.name AS editor');
		$query->join('LEFT', '#__users AS u ON u.id = a.checked_out');
		
		// WHERE
		$filter_state		= $app->getUserStateFromRequest( $option . 'pl_filter_state', 'filter_state', '', 'word' );
		$filter_order		= $app->getUserStateFromRequest( $option . 'pl_filter_order', 'filter_order', 'a.ordering', 'cmd' );
		$filter_order_Dir	= $app->getUserStateFromRequest( $option . 'pl_filter_order_Dir',	'filter_order_Dir', '',	'word' );
		$search				= $app->getUserStateFromRequest( $option . 'pl_search', 'search',	'', 'string');
		$search_mode		= $app->getUserStateFromRequest( $option . 'pl_search_mode', 'search_mode', '', 'string');
		$project_id			= $app->getUserState( $option . 'project' );
		$team_id			= $app->getUserState( $option . 'team_id' );
		$project_team_id	= $app->getUserState( $option . 'project_team_id' );
		$search				= JString::strtolower( $search );
		$exludePerson		= '';
		
		// WHERE - PUBLISHED
		if ($filter_state)
		{
			if ($filter_state == 'P')
			{
				$query->where('a.published = 1');
			}
			elseif ($filter_state == 'U' )
			{
				$query->where('a.published = 0');
			}
			elseif ($filter_state == 'A' )
			{
				$query->where('a.published = 2');
			}
			elseif ($filter_state == 'T' )
			{
				$query->where('a.published = -2');
			}
		}
		
		// WHERE - SEARCH
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = '.(int) substr($search, 3));
			} else {
				if ($search_mode) {
					$search = $db->Quote($db->escape($search, true).'%');
				} else {
					$search = $db->Quote('%'.$db->escape($search, true).'%');
				}
				if($search) {
					$query->where('(LOWER(a.lastname) LIKE '.$search.' OR LOWER(a.firstname) LIKE '.$search.' OR LOWER(a.nickname) LIKE '.$search.')');
				}
			}
		}
		
		if ($filter_order == 'a.ordering')
		{
			$query->order(array($db->escape('a.ordering '.$filter_order_Dir)));
		}
		else
		{
			$query->order(array($db->escape($filter_order.' '.$filter_order_Dir),'a.ordering'));
		}
		
		return $query;
	}

	
	/**
	 * get person history across all projects, with team, season, position,... info
	 *
	 * @param int $person_id
	 * @param int $order ordering for season and league
	 * @param string $filter e.g. "s.name = 2007/2008", default empty string
	 * @return array of objects
	 */
	function jl_getPersonHistory( $person_id, $order = 'ASC', $published = 1, $filter = "" )
	{
		if ( $published )
		{
			$filter .= " AND p.published = 1 ";
		}

		$query = "	SELECT	pt.id AS ptid,
							pt.person_id AS pid,
							pt.team_id, pt.project_id,
							t.name AS teamname,
							p.name AS pname,
							s.name AS sname,
							tt.id AS ttid,
							pos.name AS position
					FROM #__joomleague_team_player AS pt
					INNER JOIN #__joomleague_project AS p ON p.id = pt.project_id
					INNER JOIN #__joomleague_season AS s ON s.id = p.season_id
					INNER JOIN #__joomleague_league AS l ON l.id = p.league_id
					INNER JOIN #__joomleague_team AS t ON t.id = pt.team_id
					INNER JOIN #__joomleague_project_team AS tt ON pt.team_id = tt.team_id AND pt.project_id = tt.project_id
					INNER JOIN #__joomleague_position AS pos ON pos.id = pt.project_position_id
					WHERE person_id='" . $person_id . "' " . $filter . "
					GROUP BY pt.id	ORDER BY	s.ordering " . $order . ",
												l.ordering " . $order . ",
												p.name
									ASC";
		$this->_db->setQuery( $query );
		$result = $this->_db->loadObjectList();
		return $result;
	}

	/**
	 * get person history across all projects, with team, season, position,... info
	 *
	 * @param int $person_id , linked to person_id from person object
	 * @param int $order ordering for season and league
	 * @param string $filter e.g. "s.name = 2007/2008", default empty string
	 * @return array of objects
	 */
	function jl_getStaffHistory( $person_id, $order = 'ASC', $published = 1, $filter = "" )
	{
		if ( $published )
		{
			$filter .= " AND p.published = 1 ";
		}

		$query = "	SELECT	ts.teamstaff_id AS tsid,
							ts.person_id AS pid,
							p.id AS project_id,
							t.name AS teamname,
							p.name AS pname,
							s.name AS sname,
							tt.id AS ttid,
							pos.name AS position
					FROM	#__joomleague_team_staff AS ts
					INNER JOIN #__joomleague_project_team AS tt ON tt.id = ts.projectteam_id
					INNER JOIN #__joomleague_project AS p ON p.id = tt.project_id
					INNER JOIN #__joomleague_season AS s ON s.id = p.season_id
					INNER JOIN #__joomleague_league AS l ON l.id = p.league_id
					INNER JOIN #__joomleague_team AS t ON t.id = tt.team_id
					INNER JOIN #__joomleague_position AS pos ON pos.id = ts.project_position_id
					WHERE person_id= '" . $person_id . "' " . $filter . "
					GROUP BY ts.teamstaff_id	ORDER BY	s.ordering " . $order . ",
											l.ordering " . $order . ",
											p.name
											ASC";

		$this->_db->setQuery( $query );
		if ( !$result = $this->_db->loadObjectList() )
		{
			$this->setError( $this->_db->getErrorMsg() );
			return false;
		}
		else
		{
			return $result;
		}
	}

	/**
	 * return persons list from ids contained in var cid
	 *
	 * @return array
	 */
	function getPersonsToAssign()
	{
		$input = JFactory::getApplication()->input;
		$cid = $input->get('cid');
		if ( !count( $cid ) )
		{
			return array();
		}

		$db = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select($this->getState('item.select','a.id,a.firstname,a.nickname,a.lastname'));
		$query->from('#__joomleague_person AS a');
		$query->where(array('a.id IN ('.implode(', ', $cid).')','a.published = 1'));
		
		$db->setQuery($query);
		return $db->loadObjectList();
	}


	/**
	 * return list of project teams for select options
	 *
	 * @return array
	 */
	function getProjectTeamList()
	{
		$query = '	SELECT	t.id AS value,
							t.name AS text
					FROM #__joomleague_team AS t
					INNER JOIN	#__joomleague_project_team AS tt ON tt.team_id = t.id
					WHERE tt.project_id = ' . $this->_project_id . '
					ORDER BY text ASC ';

		$this->_db->setQuery( $query );
		return $this->_db->loadObjectList();
	}

	/**
	 * get team name
	 *
	 * @return string
	 */
	function getTeamName( $team_id )
	{
		if ( !$team_id )
		{
			return '';
		}
		$query = ' SELECT name FROM #__joomleague_team WHERE id = ' . $team_id;
		$this->_db->setQuery( $query );
		return $this->_db->loadResult();
	}

	/**
	 * get team name
	 *
	 * @return string
	 */
	function getProjectTeamName( $project_team_id )
	{
		if ( !$project_team_id )
		{
			return '';
		}
		$query = ' SELECT t.name
					FROM #__joomleague_team AS t
					INNER JOIN #__joomleague_project_team AS pt
					ON t.id=pt.team_id
					WHERE pt.id = '. $this->_db->Quote($project_team_id);
		$this->_db->setQuery( $query );
		return $this->_db->loadResult();
	}

}
?>