<?php 
/**
 * @copyright	Copyright (C) 2005-2015 joomleague.at. All rights reserved.
 * @license		GNU/GPL,see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License,and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

defined('_JEXEC') or die('Restricted access'); // Check to ensure this file is included in Joomla!

jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');

/**
 * Joomleague Component Person Controller
 */
class JoomleagueControllerPerson extends JoomleagueController
{
	protected $view_list = 'persons';

	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('add','display');
		$this->registerTask('edit','display');
		$this->registerTask('apply','save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$input = $this->input;
		switch($this->getTask())
		{
			case 'add' :
				{
					$input->set('hidemainmenu',0);
					$input->set('layout','form');
					$input->set('view','person');
					$input->set('edit',false);

					// Checkout the project
					$model=$this->getModel();
					$model->checkout();
				} break;

			case 'edit' :
				{
					$input->set('hidemainmenu',0);
					$input->set('layout','form');
					$input->set('view','person');
					$input->set('edit',true);

					// Checkout the project
					$model=$this->getModel();
					$model->checkout();
				} break;
		}
		parent::display();
	}

	public function save()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(0), 'array');
		// TODO: Try to combine the raw and filterinput commands on notes
		//       For the moment this is needed to prevent that html formatting (e.g. <p>) is removed.
		$notes = $input->post->get('notes', 'none', 'raw');
		$filter = JFilterInput::getInstance(null, null, 1, 1, 0);
		$post = $input->post->getArray();
		$post['id'] = (int) $cid[0];
		$post['notes'] = $filter->clean($notes, 'html');

		$model=$this->getModel();
		if ($pid = $model->store($post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_SAVED');

			if ($input->get('assignperson'))
			{
				$project_team_id    = $input->getInt('team_id',0);

				$model=$this->getModel('teamplayers');
				if ($model->storeassigned(array($pid), $project_team_id))
				{
					$msg .= ' - '.JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_PERSON_ASSIGNED');
				}
				else
				{
					$msg .= ' - '.JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_ERROR_PERSON_ASSIGNED').$model->getError();
				}
				$model=$this->getModel();
			}
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_ERROR_SAVE').$model->getError();
		}

		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ($this->getTask() == 'save')
		{
			$link='index.php?option='.$this->option.'&view='.$this->view_list;
		}
		else
		{
			$link='index.php?option='.$this->option.'&task=person.edit&cid[]='.$post['id'];
		}
		#echo $msg;
		$this->setRedirect($link,$msg);
	}

	// save the checked rows inside the persons list
	public function saveshort()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		$model=$this->getModel();
		if ($model->storeshort($cid, $input->post->getArray()))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_PERSON_UPDATE');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_ERROR_PERSON_UPDATE').$model->getError();
		}
		#echo $msg;
		$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=person.display';
		$this->setRedirect($link,$msg);
	}

	public function remove()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_DELETE'));
		}
		$model=$this->getModel();
		if(!$model->delete($cid))
		{
			$this->setRedirect('index.php?option='.$this->option.'&view=persons&task=person.display',$model->getError(),'error');
			return;
		}
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=person.display');
	}

	public function cancel()
	{
		// Checkin the project
		$model=$this->getModel();
		$model->checkin();
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=person.display');
	}

	//FIXME can it be removed?
	public function assign()
	{
		$app	= JFactory::getApplication();
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		$input->set('hidemainmenu',1);
		$input->set('layout','assignconfirm');
		$input->set('view','persons');
		$input->set('project_id', $app->getUserState($this->option.'project',0));
		$input->set('pid', $ids[0]);
		// Checkout the project
		$model=$this->getModel('teamplayers');
		parent::display();
	}

	public function saveassigned()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		// Or use $input->post->get?
		$project_team_id	= $input->getInt('project_team_id',0);
		$pid 				= $input->get('cid',array(),'array');
		$type				= $input->getInt('type',0);
		$project_id			= $input->getInt('project_id',0);
		JArrayHelper::toInteger($pid);
		if ($type == 0)
		{ //players
			$model=$this->getModel('teamplayers');

			if ($model->storeassigned($pid, $project_team_id))
			{
				$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_PERSON_ASSIGNED_AS_PLAYER');
			}
			else
			{
				$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_ERROR_PERSON_ASSIGNED_AS_PLAYER').$model->getError();
			}
			$link='index.php?option='.$this->option.'&view=teamplayers&task=teamplayer.display';
		}
		elseif ($type == 1)
		{ //staff
			$model=$this->getModel('teamstaffs');

			if ($model->storeassigned($pid, $project_team_id))
			{
				$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_PERSON_ASSIGNED_AS_STAFF');
			}
			else
			{
				$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_ERROR_PERSON_ASSIGNED_AS_STAFF').$model->getError();
			}
			$link='index.php?option='.$this->option.'&view=teamstaffs&task=teamstaff.display';
		}
		elseif ($type == 2)
		{ //referee
			$model=$this->getModel('projectreferees');

			if ($model->storeassigned($pid, $project_id))
			{
				$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_PERSON_ASSIGNED_AS_REFEREE');
			}
			else
			{
				$msg=JText::_('COM_JOOMLEAGUE_ADMIN_PERSON_CTRL_ERROR_PERSON_ASSIGNED_AS_REFEREE').$model->getError();
			}
			$link='index.php?option='.$this->option.'&view=projectreferees&task=projectreferee.display';
		}
		#echo $msg;
		$this->setRedirect($link,$msg);
	}

	// view,layout are settend in link request,to be changed?
	public function personassign()
	{
		$input = $this->input;
		$input->set('hidemainmenu',1);
		$input->set('layout','assignperson');
		parent::display();
	}

	public function select()
	{
		$app = JFactory::getApplication();
		$input = $this->input;
		$input->set('team_id', $input->get('team'));
		$input->set('task', 'teamplayers');
		$app->setUserState($this->option.'team_id', $input->get('team_id'));
		$app->setUserState($this->option.'task', $input->get('task'));
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=person.display&layout=teamplayers');
	}

	public function import()
	{
		$input = $this->input;
		$input->set('view', 'import');
		$input->set('table', 'person');
		parent::display();
	}

	public function export()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_EXPORT'));
		}
		$model = $this->getModel();
		$model->export($cid, "person", "Person");
	}
	
	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'Person', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>