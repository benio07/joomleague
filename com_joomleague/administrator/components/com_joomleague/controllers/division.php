<?php
/**
* @copyright	Copyright (C) 2006-2015 joomleague.at. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

/**
 * Joomleague Component Controller
 *
 * @package		Joomleague
 * @since 0.1
 */
class JoomleagueControllerDivision extends JoomleagueController
{
	protected $view_list = 'divisions';
	
	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask( 'add', 'display' );
		$this->registerTask( 'edit', 'display' );
		$this->registerTask( 'apply', 'save' );
	}

	public function display($cachable = false, $urlparams = false)
	{
		$app	= JFactory::getApplication();
		$option = $this->option;
		$input  = $this->input;
		$document = JFactory::getDocument();

		switch ( $this->getTask() )
		{
			case 'add'	 :
			{
				$model = $this->getModel();
				$viewType = $document->getType();
				$view = $this->getView( 'division', $viewType );
				$view->setModel( $model, true );	// true is for the default model;

				$projectws = $this->getModel( 'project' );
				$projectws->setId( $app->getUserState( $option . 'project', 0 ) );
				$view->setModel( $projectws );

				$input->set( 'hidemainmenu', 0 );
				$input->set( 'layout', 'form' );
				$input->set( 'view', 'division' );
				$input->set( 'edit', false );

				// Checkout the project
				$model->checkout();
			} break;

			case 'edit'	:
			{
				$model = $this->getModel ();
				$viewType = $document->getType();
				$view = $this->getView( 'division', $viewType );
				$view->setModel( $model, true );	// true is for the default model;

				$projectws = $this->getModel( 'project' );
				$projectws->setId( $app->getUserState( $option . 'project', 0 ) );
				$view->setModel( $projectws );

				$input->set( 'hidemainmenu', 0 );
				$input->set( 'layout', 'form' );
				$input->set( 'view', 'division' );
				$input->set( 'edit', true );

				// Checkout the project
				$model->checkout();

			} break;

			default :
			{
				$model = $this->getModel( 'divisions' );
				$viewType = $document->getType();
				$view = $this->getView( 'divisions', $viewType );
				$view->setModel( $model, true );	// true is for the default model;

				$projectws = $this->getModel( 'project' );
				
				$projectws->setId( $app->getUserState( $option . 'project', 0 ) );
				$view->setModel( $projectws );
			}
			break;

		}
		parent::display();
	}

	// save division in cid and save/update also the events associated with the saved division
	public function save()
	{
		// Check for request forgeries
		JSession::checkToken() or die( 'COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN' );

		$input = $this->input;
		$cid = $input->post->get('cid', array(0), 'array');
		// TODO: Try to combine the raw and filterinput commands on notes
		//       For the moment this is needed to prevent that html formatting (e.g. <p>) is removed.
 		$notes = $input->post->get('notes', 'none', 'raw');
 		$filter = JFilterInput::getInstance(null, null, 1, 1, 0);
 		$post = $input->post->getArray();
		$post['id'] = (int) $cid[0];
		$post['notes'] = $filter->clean($notes, 'html');

		$model = $this->getModel();
		if ( $model->store( $post ) )
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_DIVISION_CTRL_SAVED' );
		}
		else
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_DIVISION_CTRL_ERROR_SAVE' ) . $model->getError();
		}

		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();

		if ( $this->getTask() == 'save' )
		{
			$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=division.display';
		}
		else
		{
			$link = 'index.php?option='.$this->option.'&task=division.edit&cid[]=' . $post['id'];
		}

		$this->setRedirect( $link, $msg );
	}

	// remove the divisions in cid
	public function remove()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if ( count( $cid ) < 1 )
		{
			JError::raiseError( 500, JText::_( 'COM_JOOMLEAGUE_GLOBAL_SELECT_TO_DELETE' ) );
		}

		// TODO: add ACL here...
		
		$model = $this->getModel();

		if ( !$model->delete( $cid ) )
		{
			echo "<script> alert('" . $model->getError(true) . "'); window.history.go(-1); </script>\n";
		}

		$this->setRedirect( 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=division.display' );
	}

	public function cancel()
	{
		// Checkin the project
		$model = $this->getModel();
		$model->checkin();

		$this->setRedirect( 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=division.display' );
	}
	
	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'Division', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>