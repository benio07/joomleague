<?php
/**
* @copyright	Copyright (C) 2006-2015 joomleague.at. All rights reserved.
* @license		GNU/GPL,see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License,and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Joomleague Component Controller
 *
 * @package	JoomLeague
 * @since	0.1
 */
class JoomleagueControllerTeamSTaff extends JoomleagueController
{
	protected $view_list = 'teamstaffs';

	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('add','display');
		$this->registerTask('edit','display');
		$this->registerTask('apply','save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$document = JFactory::getDocument();
		$model=$this->getModel('teamstaffs');
		$viewType=$document->getType();
		$view=$this->getView('teamstaffs',$viewType);
		$view->setModel($model,true);  // true is for the default model;

		$app = JFactory::getApplication();
		$projectws=$this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project',0));
		$view->setModel($projectws);

		$teamws=$this->getModel('projectteam');
		$teamws->setId($app->getUserState($this->option.'project_team_id',0));
		$view->setModel($teamws);

		$input = $this->input;
		switch($this->getTask())
		{
			case 'add'	 :
				{
					$input->set('hidemainmenu',$input->get('hidemainmenu',0));
					$input->set('layout','form');
					$input->set('view','teamstaff');
					$input->set('edit',false);

					//$model=$this->getModel();
					//$model->checkout();
				} break;

			case 'edit'	:
				{
					$model=$this->getModel();
					$viewType=$document->getType();
					$view=$this->getView('teamstaff',$viewType);
					$view->setModel($model,true);  // true is for the default model;

					$projectws->setId($app->getUserState($this->option.'project',0));
					$view->setModel($projectws);

					$teamws=$this->getModel('projectteam');
					$teamws->setId($app->getUserState($this->option.'project_team_id',0));
					$view->setModel($teamws);

					$input->set('hidemainmenu',$input->get('hidemainmenu',0));
					$input->set('layout','form');
					$input->set('view','teamstaff');
					$input->set('edit',true);

					// Checkout the project
 					//$model=$this->getModel();
					//$model->checkout();
				} break;

		}
		parent::display($cachable, $urlparams);
	}

	public function editlist()
	{
		$document = JFactory::getDocument();
		$model=$this->getModel('teamstaffs');
		$viewType=$document->getType();
		$view=$this->getView('teamstaffs',$viewType);
		$view->setModel($model,true);  // true is for the default model;

		$app = JFactory::getApplication();
		$projectws=$this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project',0));
		$view->setModel($projectws);
		
		$teamws=$this->getModel('projectteam');
		$teamws->setId($app->getUserState($this->option.'project_team_id',0));
		$view->setModel($teamws);

		$input = $this->input;
		$input->set('hidemainmenu',$input->get('hidemainmenu',1));
		$input->set('layout','editlist');
		$input->set('view','teamstaffs');
		$input->set('edit',true);

		parent::display();
	}

	public function save_teamstaffslist()
	{
		$input = $this->input;
		$cid = $input->post->get('cid',array(0),'array');
		$project=$input->post->get('project');
		$post = $input->post->getArray();
		$post['id']=(int)$cid[0];
		$post['project_id']=$project;
		$model=$this->getModel('teamstaffs');
		if ($model->store($post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TEAMSTAFF_CTRL_TEAMSTAFF_SAVED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TEAMSTAFF_CTRL_ERROR_TEAMSTAFF_SAVED').$model->getError();
		}
		// Check the table in so it can be edited.... we are done with it anyway
		//$model->checkin();
		$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=teamstaff.display';
		$this->setRedirect($link,$msg);
	}

	public function save()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid',array(0),'array');
		// TODO: Try to combine the raw and filterinput commands on notes
		//       For the moment this is needed to prevent that html formatting (e.g. <p>) is removed.
 		$notes = $input->post->get('notes', 'none', 'raw');
 		$filter = JFilterInput::getInstance(null, null, 1, 1, 0);
		$post = $input->post->getArray();
		$post['id'] = (int) $cid[0];
		$post['notes'] = $filter->clean($notes, 'html');
		$model = $this->getModel();
		if ($model->store($post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TEAMSTAFF_CTRL_SAVED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TEAMSTAFF_CTRL_ERROR_SAVE').$model->getError();
		}
		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ($this->getTask()=='save')
		{
			$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=teamstaff.display';
		}
		else
		{
			$link='index.php?option='.$this->option.'&task=teamstaff.edit&cid[]='.$post['id'];
		}
		#echo $msg;
		$this->setRedirect($link,$msg);
	}

	// save the checked rows inside the project teams list
	public function saveshort()
	{
		$input = $this->input;
		$post = $input->post->getArray();
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);

		$model=$this->getModel('teamstaffs');
		if ($model->storeshort($cid,$post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TEAMSTAFF_CTRL_UPDATED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TEAMSTAFF_CTRL_ERROR_UPDATED').$model->getError();
		}
		$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=teamstaff.display';
		$this->setRedirect($link,$msg);
	}

	public function remove()
	{
		$app = JFactory::getApplication();
		$project_id = $app->getUserState($this->option.'project',0);
		$input = $this->input;
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_DELETE'));
		}
		// Access checks.
		$user = JFactory::getUser();
		foreach ($cid as $i => $id)
		{
			if (!$user->authorise('core.admin', 'com_joomleague') ||
				!$user->authorise('core.admin', 'com_joomleague.project.'.(int) $project_id) ||
				!$user->authorise('core.delete', 'com_joomleague.team_staff.'.(int) $id))
			{
				// Prune items that you can't delete.
				unset($cid[$i]);
				JError::raiseNotice(403, JText::_('JERROR_CORE_DELETE_NOT_PERMITTED'));
			}
		}
		$model=$this->getModel();
		if(!$model->delete($cid))
		{
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=teamstaff.display');
	}

	public function select()
	{
		$input = $this->input;
		$app = JFactory::getApplication();
		$app->setUserState($this->option.'project_team_id', $input->get('project_team_id'));
		$app->setUserState($this->option.'team_id', $input->get('team_id'));
		$app->setUserState($this->option.'team', $input->get('project_team_id'));
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=teamstaff.display');
	}

	public function assign()
	{
		//redirect to teamstaffs page,with a message
		$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TEAMSTAFF_CTRL_ASSIGN');
		$this->setRedirect('index.php?option='.$this->option.'&view=persons&task=person.display&layout=assignplayers&type=1&hidemainmenu=1',$msg);
	}

	public function unassign()
	{
		$input = $this->input;
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);
		$model=$this->getModel('teamstaffs');
		$nDeleted=$model->remove($cid);
		if ($nDeleted!=count($cid))
		{
			$msg=JText::sprintf('COM_JOOMLEAGUE_ADMIN_TEAMSTAFF_CTRL_UNASSIGN',$nDeleted);
			$msg .= '<br/>'.$model->getError();
			$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=teamstaff.display',$msg,'error');
		}
		else
		{
			$msg=JText::sprintf('COM_JOOMLEAGUE_ADMIN_TEAMSTAFF_CTRL_UNASSIGN',$nDeleted);
			$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=teamstaff.display',$msg);
		}
	}
	
	public function cancel()
	{
		// Checkin the template
		$model=$this->getModel();
		$model->checkin();
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=teamstaff.display');
	}

	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'TeamStaff', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>