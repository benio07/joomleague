<?php
/**
 * @copyright	Copyright (C) 2005-2015 joomleague.at. All rights reserved.
 * @license		GNU/GPL,see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License,and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');

/**
 * Joomleague Component Club Controller
 *
 * @package	JoomLeague
 * @since	0.1
 */
class JoomleagueControllerClub extends JoomleagueController
{
	protected $view_list = 'clubs';
	
	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('add','display');
		$this->registerTask('edit','display');
		$this->registerTask('apply','save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$input = $this->input;
		$hideMainMenu = $input->get('hidemainmenu',0);
		switch($this->getTask())
		{
			case 'add'     :
				{
					$input->set('hidemainmenu', $hideMainMenu);
					$input->set('layout','form');
					$input->set('view','club');
					$input->set('edit',false);

					// Checkout the club
					$model=$this->getModel();
					$model->checkout();
				} break;
			case 'edit'    :
				{
					$input->set('hidemainmenu', $hideMainMenu);
					$input->set('layout','form');
					$input->set('view','club');
					$input->set('edit',true);

					// Checkout the club
					$model=$this->getModel();
					$model->checkout();
				} break;
		}
		parent::display();
	}

	public function save()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(0), 'array');
		// TODO: Try to combine the raw and filterinput commands on notes
		//       For the moment this is needed to prevent that html formatting (e.g. <p>) is removed.
 		$notes = $input->post->get('notes', 'none', 'raw');
 		$filter = JFilterInput::getInstance(null, null, 1, 1, 0);
 		$cpost = $input->post->getArray();
		$cpost['id'] = (int) $cid[0];
		$cpost['notes'] = $filter->clean($notes, 'html');

		$msg='';
		$model=$this->getModel();
		if ($clubid = $model->store($cpost))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_CLUB_CTRL_SAVED');
			$createTeam=$input->get('createTeam');
			if ($createTeam)
			{
				$team_name=$input->get('name');
				$team_short_name=strtoupper(substr(preg_replace("/[^a-zA-Z]/","",$team_name),0,3));
				$tpost['id']= "0";
				$tpost['name']= $team_name;
				// middle_name is not set here, as there is no suitable algorithm that suits all JL users.
				$tpost['short_name']= $team_short_name ;
				$tpost['club_id']= $clubid;
				$teammodel=$this->getModel('team');
				$teammodel->store($tpost);
			}
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_CLUB_CTRL_ERROR_SAVE').$model->getError();
		}
		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ($this->getTask()=='save')
		{
			$link='index.php?option='.$this->option.'&view='.$this->view_list.'&club.display';
		}
		else
		{
			$link='index.php?option='.$this->option.'&task=club.edit&cid[]='.$cpost['id'];
		}
		$link .= '&hidemainmenu='.$input->get('hidemainmenu',0);
		$this->setRedirect($link,$msg);
	}

	public function remove()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$user = JFactory::getUser();
		$input = $this->input;
		$cid = $input->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_ADMIN_CLUB_CTRL_SELECT_TO_DELETE'));
		}
		// Access checks.
		foreach ($cid as $i => $id)
		{
			if (!$user->authorise('core.admin', 'com_joomleague') ||
				!$user->authorise('core.delete', 'com_joomleague.club.'.(int) $id))
			{
				// Prune items that you can't delete.
				unset($cid[$i]);
				JError::raiseNotice(403, JText::_('JERROR_CORE_DELETE_NOT_PERMITTED'));
			}
		}
		
		$msg='';
		$model=$this->getModel();
		if(!$model->delete($cid))
		{
			echo "<script> alert('".$model->getError()."'); window.history.go(-1); </script>\n";
			return;
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_CLUB_CTRL_DELETED');
		}
		$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=club.display';
		$link .= '&hidemainmenu='.$input->get('hidemainmenu',0);
		$this->setRedirect($link,$msg);
	}

	public function cancel()
	{
		// Checkin the club
		$model=$this->getModel();
		$model->checkin();
		$input = $this->input;
		$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=club.display';
		$link .= '&hidemainmenu='.$input->get('hidemainmenu',0);
		$this->setRedirect($link);
	}

	public function import()
	{
		$input = $this->input;
		$input->set('view','import');
		$input->set('table','club');
		parent::display();
	}

	public function export()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_EXPORT'));
		}
		$model = $this->getModel();
		$model->export($cid, "club", "Club");
	}
	
	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'Club', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>