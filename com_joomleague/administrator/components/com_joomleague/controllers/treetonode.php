<?php
/**
* @copyright	Copyright (C) 2006-2014 joomleague.at. All rights reserved.
* @license		GNU/GPL,see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License,and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Joomleague Component Controller
 *
 * @package	JoomLeague
 * @since	0.1
 */
class JoomleagueControllerTreetonode extends JoomleagueController
{
	protected $view_list = 'treetonodes';
	
	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('edit','display');
		$this->registerTask('apply','save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$document = JFactory::getDocument();
	 	$model=$this->getModel('treetonodes');
		$viewType=$document->getType();
		$view=$this->getView('treetonodes',$viewType);
		$view->setModel($model,true);	// true is for the default model;

		$app = JFactory::getApplication();
		$projectws=$this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project',0));
		$view->setModel($projectws);
		
		$input = $this->input;
		if ($tid = $input->get('tid', null, 'array'))
		{
			$app->setUserState($this->option.'treeto_id', $tid[0]);
		}
		$treetows = $this->getModel('treeto');
		$treetows->setId($app->getUserState($this->option.'treeto_id'));
		$view->setModel($treetows);
		
		switch($this->getTask())
		{
			case 'edit'	:
			{
				$model=$this->getModel('treetonode');
				$viewType=$document->getType();
				$view=$this->getView('treetonode',$viewType);
				$view->setModel($model,true);	// true is for the default model;

				$view->setModel($projectws);

				$input->set('hidemainmenu',0);
				$input->set('layout','form');
				$input->set('view','treetonode');
				$input->set('edit',true);

				$model=$this->getModel('treetonode');
				$model->checkout();
			} break;
		}
		parent::display();
	}

	public function removenode()
	{
		$app = JFactory::getApplication();
		$input = $this->input;
		$post = $input->post->getArray();
		$post['treeto_id']=$app->getUserState($this->option.'treeto_id',0);
		
		$model = $this->getModel( 'treetonodes' );
		if ( $model->setRemoveNode() )
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_REMOVENODE' );
		}
		else
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_ERROR_REMOVENODE' );
		}
		$link = 'index.php?option='.$this->option.'&view=treetos&task=treeto.display';
		$this->setRedirect( $link, $msg );
	}

	public function unpublishnode()
	{
		$model = $this->getModel( 'treetonode' );
		if ( $model->setUnpublishNode() )
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_UNPUBLISHNODE' );
		}
		else
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_ERROR_UNPUBLISHNODE' );
		}
		$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=treetonode.display';
		$this->setRedirect( $link, $msg );
	}

	// save the checked nodes inside the trees
	public function saveshortleaf()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);
		$post = $input->post->getArray();
		$app = JFactory::getApplication();
		$post['treeto_id']=$app->getUserState($this->option.'treeto_id',0);

		$model=$this->getModel('treetonodes');
		if ($model->storeshortleaf($cid,$post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_SAVED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_ERROR_SAVED').$model->getError();
		}
		$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=treetonode.display';
		$this->setRedirect($link,$msg);
	}
	public function savefinishleaf()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$post = $input->post->getArray();
		$app = JFactory::getApplication();
		$post['treeto_id']=$app->getUserState($this->option.'treeto_id',0);

		$model = $this->getModel( 'treetonodes' );
		if ( $model->storefinishleaf() )
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_LEAFS_SAVED' );
		}
		else
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_LEAFS_ERROR_SAVED' );
		}
		$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=treetonode.display';
		$this->setRedirect( $link, $msg );
	}
	// save the checked nodes inside the trees
	public function saveshort()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$post = $input->post->getArray();
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);
		
		$model=$this->getModel('treetonodes');
		if ($model->storeshort($cid,$post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_SAVED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_ERROR_SAVED').$model->getError();
		}
		$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=treetonode.display';
		$this->setRedirect($link,$msg);
	}

	public function save()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$post = $input->post->getArray();
		
		$model=$this->getModel('treetonode');
		if ($model->store($post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_SAVED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_ERROR_SAVED').$model->getError();
		}
		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ($this->getTask()=='save')
		{
			$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=treetonode.display';
		}
		else
		{
			$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=treetonode.edit&cid[]='.$post['id'];
		}
		$this->setRedirect($link,$msg);
	}

	//	assign (empty)match to node	from editmatches view
	public function assignmatch()
	{
		$app = JFactory::getApplication();
		$input = $this->input;
		$post = $input->post->getArray();
		$post['project_id']=$app->getUserState($this->option.'project',0);
		$post['node_id']=$app->getUserState($this->option.'node_id',0);
		
		$model=$this->getModel('treetonode');
		if ($model->store($post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_ADD_MATCH');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETONODE_CTRL_ERROR_ADD_MATCH').$model->getError();
		}
		$link='index.php?option='.$this->option.'&view=matches&task=match.display';
		$this->setRedirect($link,$msg);
	}

}
?>