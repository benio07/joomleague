<?php
/**
* @copyright	Copyright (C) 2006-2014 joomleague.at. All rights reserved.
* @license		GNU/GPL,see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License,and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Joomleague Component Matchday Model
 */
class JoomleagueControllerRound extends JoomleagueController
{
	protected $view_list = 'rounds';
	
	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('add','display');
		$this->registerTask('edit','display');
		$this->registerTask('apply','save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$document = JFactory::getDocument();
	 	$model=$this->getModel('rounds');
		$viewType=$document->getType();
		$view=$this->getView('rounds',$viewType);
		$view->setModel($model,true);	// true is for the default model;

		$app = JFactory::getApplication();
		$projectws=$this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project',0));
		$view->setModel($projectws);

		$input = $this->input;
		switch($this->getTask())
		{
			case 'add':
			{
				$model=$this->getModel();
				$viewType=$document->getType();
				$view=$this->getView('round',$viewType);
				$view->setModel($model,true);	// true is for the default model;

 				$projectws=$this->getModel('project');
				$projectws->setId($app->getUserState($this->option.'project',0));
				$view->setModel($projectws);

				$input->set('hidemainmenu',1);
				$input->set('layout','form');
				$input->set('view','round');
				$input->set('edit',false);

				$model=$this->getModel();
				$model->checkout();
			} break;

			case 'edit'	:
			{
				$model=$this->getModel();
				$viewType=$document->getType();
				$view=$this->getView('round',$viewType);
				$view->setModel($model,true);	// true is for the default model;

				$projectws=$this->getModel('project');
				$projectws->setId($app->getUserState($this->option.'project',0));
				$view->setModel($projectws);

				$input->set('hidemainmenu',0);
				$input->set('layout','form');
				$input->set('view','round');
				$input->set('edit',true);

				// Checkout the round
				$model=$this->getModel();
				$model->checkout();
			} break;

			case 'massadd'	:
			{
				$input->set('massadd',true);
			} break;

		}
		parent::display();
	}

	public function save()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$post = $input->post->getArray();

		$model=$this->getModel();
		// convert dates back to mysql date format
		if (isset($post['round_date_first']))
		{
			$post['round_date_first']=strtotime($post['round_date_first']) ? strftime('%Y-%m-%d',strtotime($post['round_date_first'])) : null;
		}
		else
		{
			$post['round_date_first']=null;
		}
		if (isset($post['round_date_last']))
		{
			$post['round_date_last']=strtotime($post['round_date_last']) ? strftime('%Y-%m-%d',strtotime($post['round_date_last'])) : null;
		}
		else
		{
			$post['round_date_last']=null;
		}
		$app = JFactory::getApplication();
		$max=$model->getMaxRound($app->getUserState($this->option.'project',0));
		$max++;
		if (!isset($post['roundcode']) || empty($post['roundcode']))
		{
			//$max=$model->getMaxRound($app->getUserState($this->option.'project',0));
			$post['roundcode']=$max;
		}
		if (!isset($post['name']) || empty($post['name']))
		{
			//$max=$model->getMaxRound($app->getUserState($this->option.'project',0));
			//$mrc=$max + 1;
			$post['name']=JText::sprintf('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_ROUND_NAME',$max);
		}
		if ($model->store($post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_ROUND_SAVED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_ERROR_SAVE').$model->getError();
		}
		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ($this->getTask()=='save')
		{
			$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=round.display';
		}
		else
		{
			$link='index.php?option='.$this->option.'&task=round.edit&cid[]='.$post['id'];
		}
		$this->setRedirect($link,$msg);
	}

	// save the checked rows inside the rounds list
	public function saveshort()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		$post = $input->post->getArray();
		
		$model=$this->getModel();
		for ($x=0; $x < count($cid); $x++)
		{
			$post['round_date_first'.$cid[$x]]=JoomleagueHelper::convertDate($post['round_date_first'.$cid[$x]],0);
			$post['round_date_last'.$cid[$x]]=JoomleagueHelper::convertDate($post['round_date_last'.$cid[$x]],0);
			if (isset($post['roundcode'. $cid[$x]]))
			{
				if ($post['roundcode'.$cid[$x]]=='0')
				{
					$app = JFactory::getApplication();
					$max=$model->getMaxRound($app->getUserState($this->option.'project',0));
					$post['roundcode'.$cid[$x]]=$max + 1;
				}
			}
		}
		if ($model->storeshort($cid,$post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_ROUND_SAVED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_ERROR_SAVE').$model->getError();
		}
		$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=round.display';
		$this->setRedirect($link,$msg);
	}

	public function remove()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_DELETE'));
		}
		$mdlMatches=$this->getModel('matches');
		$mdlMatch  =$this->getModel('match');
		$model=$this->getModel();
		if (!$model->delete($cid,$mdlMatches,$mdlMatch))
		{
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}
		$msg=JText::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_DELETED');
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=round.display',$msg);
	}

	public function deletematches()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_SELECT_TO_DELETE_MATCHES'));
		}
		$mdlMatches=$this->getModel('matches');
		$mdlMatch=$this->getModel('match');
		$model=$this->getModel();
		if (!$model->deleteMatches($cid,$mdlMatches,$mdlMatch,true))
		{
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}
		$msg=JText::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_MATCHES_DELETED');
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=round.display',$msg);
	}

	public function cancel()
	{
		// Checkin the project
		$model=$this->getModel('rounds');
		
		// @todo: check
		/* $model->checkin(); */
		
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=round.display');
	}

	public function copyfrom()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$post = $input->post->getArray();
		
		$model=$this->getModel();
		$add_round_count=(int)$post['add_round_count'];
		$max=0;
		if ($add_round_count > 0) // Only MassAdd a number of new and empty rounds
		{
			$max=$model->getMaxRound($post['project_id']);
			$max++;
			$i=0;
			for ($x=0; $x < $add_round_count; $x++)
			{
				$i++;
				$post['roundcode']=$max;
				$post['name']=JText::sprintf('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_ROUND_NAME',$max);
				if ($model->store($post))
				{
					$msg=JText::sprintf('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_ROUNDS_ADDED',$i);
				}
				else
				{
					$msg=JText::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_CTRL_ERROR_ADD').$model->getError();
				}
				$max++;
			}
		}
		$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=round.display';
		$this->setRedirect($link,$msg);
	}

	/**
	 * display the populate form
	 *
	 */
	public function populate()
	{
		$document = JFactory::getDocument();
		$model    = $this->getModel();
		$viewType = $document->getType();
		$view     = $this->getView('rounds',$viewType);
		$view->setModel($model,true);	// true is for the default model;

		$app = JFactory::getApplication();
		$projectws=$this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project',0));
		$view->setModel($projectws);

		$input = $this->input;
		$input->set('hidemainmenu',0);
		$input->set('view','rounds');
		$input->set('layout','populate');

		parent::display();
	}

	/**
	 * does the populate operation
	 */
	public function startpopulate()
	{
		$msgType = 'message';
		$model = $this->getModel('rounds');
		$input = $this->input;
		$project_id = $input->get('project_id');
		$scheduling = $input->post->get('scheduling', '', 'string');
		$time       = $input->get('time');
		$interval   = $input->getInt('interval');
		$start      = $input->get('start');
		$roundname  = $input->get('roundname');
		$matchnumber= $input->get('matchnumber');
		
		$teamsorder = $input->post->get('teamsorder', array(), 'array');
		JArrayHelper::toInteger($teamsorder);
		if($scheduling == 0 || $scheduling == 1)
		{
			$bSuccess = $model->populate($project_id, $scheduling, $time, $interval, $start, $roundname, $teamsorder, $matchnumber);
		} else
		{
			$bSuccess = $model->populateByTemplate($project_id, $scheduling, $time, $interval, $start, $roundname, $teamsorder, $matchnumber);
		}
		if ($bSuccess)
		{
			$msg = Jtext::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_POPULATE_SUCCESSFULL');
		}
		else
		{
			$msg = Jtext::_('COM_JOOMLEAGUE_ADMIN_ROUNDS_POPULATE_ERROR'.': '.$model->getError());
			$msgType = 'error';
		}
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=round.display', $msg, $msgType);
	}
	
	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'Round', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>
