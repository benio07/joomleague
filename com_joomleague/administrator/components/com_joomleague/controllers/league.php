<?php
/**
* @copyright	Copyright (C) 2005-2015 joomleague.at. All rights reserved.
* @license		GNU/GPL,see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License,and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Joomleague Component League Controller
 *
 * @package	JoomLeague
 * @since	0.1
 */
class JoomleagueControllerLeague extends JoomleagueController
{
	protected $view_list = 'leagues';

	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('add','display');
		$this->registerTask('edit','display');
		$this->registerTask('apply','save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$input = $this->input;
		switch ($this->getTask())
		{
			case 'add'	 :
			{
				$input->set('hidemainmenu',0);
				$input->set('layout','form');
				$input->set('view','league');
				$input->set('edit',false);
				// Checkout the project
				$model=$this->getModel();
				$model->checkout();
			} break;
			case 'edit'	:
			{
				$input->set('hidemainmenu',0);
				$input->set('layout','form');
				$input->set('view','league');
				$input->set('edit',true);
				// Checkout the project
				$model=$this->getModel();
				$model->checkout();
			} break;
		}
		parent::display();
	}

	public function save()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(0), 'array');
		$post = $input->post->getArray();
		$post['id'] = (int) $cid[0];
		
		$model=$this->getModel();
		if ($model->store($post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_LEAGUE_CTRL_SAVED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_LEAGUE_CTRL_ERROR_SAVE').$model->getError();
		}
		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ($this->getTask()=='save')
		{
			$link='index.php?option='.$this->option.'&view='.$this->view_list;
		}
		else
		{
			$link='index.php?option='.$this->option.'&task=league.edit&cid[]='.$post['id'];
		}
		$this->setRedirect($link,$msg);
	}

	public function remove()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_DELETE'));
		}
		// TODO: add ACL...
		$model=$this->getModel();
		if (!$model->delete($cid))
		{
			echo "<script> alert('".$model->getError()."'); window.history.go(-1); </script>\n";
			return;
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_LEAGUE_CTRL_DELETED');
		}
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=league.display',$msg);
	}

	public function cancel()
	{
		// Checkin the project
		$model=$this->getModel();
		$model->checkin();
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=league.display');
	}

	public function import()
	{
		$input = $this->input;
		$input->set('view','import');
		$input->set('table','league');
		parent::display();
	}
	
	public function export()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_EXPORT'));
		}
		$model = $this->getModel();
		$model->export($cid, "league", "League");
	}
	
	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'League', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>