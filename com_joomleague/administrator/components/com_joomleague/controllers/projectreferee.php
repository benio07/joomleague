<?php
/**
* @copyright	Copyright (C) 2006-2014 joomleague.at. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.controller' );

/**
 * Joomleague Component Controller
 *
 * @author	Kurt Norgaz
 * @package	Joomleague
 * @since	1.5.02a
 */
class JoomleagueControllerProjectReferee extends JoomleagueController
{

	protected $view_list = 'projectreferees';
	
	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask( 'add', 'display' );
		$this->registerTask( 'edit', 'display' );
		$this->registerTask( 'apply', 'save' );
	}

	public function display($cachable = false, $urlparams = false)
	{
		$document	= JFactory::getDocument();
		// TODO: why do we need to do something with projectreferees (plural) model here?
		$model		= $this->getModel('projectreferees');
		$viewType	= $document->getType();
		$view		= $this->getView('projectreferees', $viewType);
		$view->setModel( $model, true );  // true is for the default model;

		$app	= JFactory::getApplication();
		$projectws	= $this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project', 0));
		// TODO: can we couple two models to one view? 
		$view->setModel($projectws);

		$input = $this->input;
		switch($this->getTask())
		{
			case 'add'	 :
				{
					$input->set('hidemainmenu', $input->get('hidemainmenu',0));
					$input->set('layout', 'form');
					$input->set('view', 'projectreferee');
					$input->set('edit', false);

					// Checkout the project
					$model = $this->getModel();
					$model->checkout();
				} break;

			case 'edit'	:
				{
					$model = $this->getModel();
					$viewType = $document->getType();
					$view = $this->getView('projectreferee', $viewType );
					$view->setModel( $model, true );  // true is for the default model;
					// TODO: set 2 models for view?
					$view->setModel( $projectws );

					$input->set('hidemainmenu', $input->get('hidemainmenu',0));
					$input->set('layout', 'form');
					$input->set('view', 'projectreferee');
					$input->set('edit', true);

					// Checkout the project
					$model = $this->getModel();
					$model->checkout();
				} break;

		}
		parent::display();
	}

	public function editlist()
	{
		$document = JFactory::getDocument();
		$model = $this->getModel('projectreferees');
		$viewType = $document->getType();
		$view = $this->getView('projectreferees', $viewType);
		$view->setModel($model, true);  // true is for the default model;

		$app	= JFactory::getApplication();
		$projectws = $this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project', 0));
		$view->setModel($projectws);

		// TODO: where does $teamws come from?
		$teamws->setId($app->getUserState( $this->option.'team', 0));
		$view->setModel($teamws);

		$input->set('hidemainmenu',$input->set('hidemainmenu',1));
		$input->set('layout', 'editlist');
		$input->set('view', 'projectreferees');
		$input->set('edit', true );

		parent::display();
	}


	public function save_projectrefereeslist()
	{
		JSession::checkToken() or die( 'COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN' );
		$input = $this->input;
		$cid = $input->post->get('cid', array(0), 'array');
		$project = $input->post->get('project');
		$team_id = $input->post->get('team');
		$post = $input->post->getArray();
		$post['id'] 		= (int) $cid[0];
		$post['project_id']	= (int) $project;
		$post['team_id']   	= (int) $team_id;
		
		$model = $this->getModel( 'projectreferees' );
		if ( $model->store( $post ) )
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_SAVED' );
		}
		else
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_ERROR_SAVE' ) . $model->getError();
		}

		$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectreferee.display';
		$this->setRedirect( $link, $msg );
	}

	public function save()
	{
		// Check for request forgeries
		JSession::checkToken() or die( 'COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN' );
		$input = $this->input;
		// TODO: The view does not define hidden input 'cid', but uses 'id'. Do we need to align?
// 		$cid = $input->post->get('cid', array(0), 'array');
		// TODO: Try to combine the raw and filterinput commands on notes
		//       For the moment this is needed to prevent that html formatting (e.g. <p>) is removed.
		$notes = $input->post->get('notes', 'none', 'raw');
		$filter = JFilterInput::getInstance(null, null, 1, 1, 0);
		$post = $input->post->getArray();
// 		$post['id'] = (int) $cid[0];
		$post['notes'] = $filter->clean($notes, 'html');

		$model = $this->getModel();
		if ( $model->store( $post ) )
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_SAVED' );
		}
		else
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_ERROR_SAVE' ) . $model->getError();
		}

		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ( $this->getTask() == 'save' )
		{
			$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectreferee.display';
		}
		else
		{
			$link = 'index.php?option='.$this->option.'&task=projectreferee.edit&cid[]=' . $post['id'];
		}
		$this->setRedirect( $link, $msg );
	}

	// save the checked rows inside the project teams list
	public function saveshort()
	{
		JSession::checkToken() or die( 'COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN' );
		$input = $this->input;
		$post = $input->post->getArray();
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger( $cid );
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_SELECT_TO_SAVE'));
		}
		
		$model = $this->getModel( 'projectreferees' );
		if ( $model->saveshort( $cid, $post ) )
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_UPDATED' );
		}
		else
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_ERROR_UPDATED' ) . $model->getError();
		}

		$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectreferee.display';
		$this->setRedirect( $link, $msg );
	}

	public function remove()
	{
		$app = JFactory::getApplication();
		$project_id = $app->getUserState($this->option.'project',0);
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger( $cid );
		if ( count( $cid ) < 1 )
		{
			JError::raiseError(500, JText::_( 'COM_JOOMLEAGUE_GLOBAL_SELECT_TO_DELETE' ) );
		}
		
		// Access checks.
		$user = JFactory::getUser();
		foreach ($cid as $i => $id)
		{
			if (!$user->authorise('core.admin', 'com_joomleague') ||
				!$user->authorise('core.admin', 'com_joomleague.project.'.(int) $project_id) || 
				!$user->authorise('core.delete', 'com_joomleague.project_referee.'.(int) $id))
			{
				// Prune items that you can't delete.
				unset($cid[$i]);
				JError::raiseNotice(403, JText::_('JERROR_CORE_DELETE_NOT_PERMITTED'));
			}
		}
		// TODO: Why using the team model here, and delete an item there...? 
		$model = $this->getModel( 'team' );
		if( !$model->delete($cid) )
		{
			echo "<script> alert('" . $model->getError( true ) . "'); window.history.go(-1); </script>\n";
		}

		$this->setRedirect( 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectreferee.display');
	}

	public function cancel()
	{
		// Checkin the project
		$model = $this->getModel();
		$model->checkin();
		$this->setRedirect( 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectreferee.display');
	}

	public function select()
	{
		$app	= JFactory::getApplication();
		$app->setUserState($this->option.'team', $this->input->get('team'));
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectreferee.display');
	}

	public function assign()
	{
		//redirect to ProjectReferees page, with a message
		$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_ASSIGN' );
		$this->setRedirect( 'index.php?option='.$this->option.'&view=persons&task=person.display&layout=assignplayers&type=2&hidemainmenu=1', $msg);
	}

	public function unassign()
	{
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger( $cid );
		if ( count( $cid ) < 1 )
		{
			JError::raiseError(500, JText::_( 'COM_JOOMLEAGUE_P_REFEREE_SELECT_TO_UNASSIGN' ) );
		}
		
		$model = $this->getModel( 'projectreferees' );
		$nDeleted = $model->unassign( $cid );
		if ( !$nDeleted )
		{
			$msg = JText::_( 'COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_UNASSIGN' );
		}
		else
		{
			$msg = JText::sprintf( 'COM_JOOMLEAGUE_ADMIN_P_REFEREE_CTRL_UNASSIGNED', $nDeleted );
		}
		//redirect to projectreferee page, with a message
		$this->setRedirect( 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectreferee.display', $msg );
	}
	
	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'Projectreferee', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
	
}
?>