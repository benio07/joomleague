<?php
/**
* @copyright	Copyright (C) 2006-2014 joomleague.at. All rights reserved.
* @license		GNU/GPL,see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License,and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Joomleague Component Model
 *
 * @package	JoomLeague
 * @since	0.1
 */
class JoomleagueControllerTreetomatch extends JoomleagueController
{
	protected $view_list = 'treetomatchs';
	
	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('add','display');
		$this->registerTask('edit','display');
		$this->registerTask('apply','save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$document = JFactory::getDocument();
	 	$model=$this->getModel('treetomatchs');
		$viewType=$document->getType();
		$view=$this->getView('treetomatchs',$viewType);
		$view->setModel($model,true);	// true is for the default model;

		$app = JFactory::getApplication();
		$projectws=$this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project',0));
		$view->setModel($projectws);
		$input = $this->input;
		if ($nid = $input->get('nid', null, 'array'))
		{
			$app->setUserState( $this->option.'node_id', $nid[0] );
		}
		if ($tid = $input->get('tid', null, 'array'))
		{
			$app->setUserState( $this->option.'treeto_id', $tid[0] );
		}
		$nodews = $this->getModel( 'treetonode' );
		$nodews->setId( $app->getUserState( $this->option.'node_id') );
		$view->setModel( $nodews );
		
		switch($this->getTask())
		{
			case 'edit'	:
			{
				$model=$this->getModel('treetomatch');
				$viewType=$document->getType();
				$view=$this->getView('treetomatch',$viewType);
				$view->setModel($model,true);	// true is for the default model;
				$view->setModel($projectws);
				
				$input->set('hidemainmenu',0);
				$input->set('layout','form');
				$input->set('view','treetomatch');
				$input->set('edit',true);

				$model=$this->getModel('treetomatch');
				$model->checkout();
			} break;

			case 'matchadd':
			{
				$input->set('matchadd',true);
			} break;

		}
		parent::display();
	}

	public function editlist()
	{
		$document	= JFactory::getDocument();
		$model		= $this->getModel('treetomatchs');
		$viewType	= $document->getType();
		$view		= $this->getView  ('treetomatchs', $viewType);
		$view->setModel($model, true);  // true is for the default model;

		$app	= JFactory::getApplication();
		$projectws = $this->getModel('project');
		$projectws->setId($app->getUserState($this->option . 'project', 0));
		$view->setModel($projectws);
		
		$input = $this->input;
		if ( $nid = $input->get( 'nid', null, 'array' ) )
		{
			$app->setUserState( $this->option.'node_id', $nid[0] );
		}
		if ( $tid = $input->get( 'tid', null, 'array' ) )
		{
			$app->setUserState( $this->option.'treeto_id', $tid[0] );
		}
		$nodews = $this->getModel( 'treetonode' );
		$nodews->setId( $app->getUserState( $this->option.'node_id') );
		$view->setModel( $nodews );
		
		$input->set('hidemainmenu', 0);
		$input->set('layout', 'editlist' );
		$input->set('view', 'treetomatchs');
		$input->set('edit', true);

		// Checkout the project
		//	$model = $this->getModel('treetomatchs');

		parent::display();
	}

	public function save_matcheslist()
	{
		$input = $this->input;
		$cid = $input->post->get('cid',array(0),'array');
		$post = $input->post->getArray();
		$post['id'] = (int) $cid[0];

		$model = $this->getModel('treetomatchs');
		if ($model->store($post))
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_TREETOMATCH_CTRL_SAVED');
		}
		else
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_TREETOMATCH_CTRL_ERROR_SAVE') . $model->getError();
		}

		// Check the table in so it can be edited.... we are done with it anyway
		//$model->checkin();
		$link = 'index.php?option='.$this->option.'&view=treetonodes&task=treetonode.display';
		$this->setRedirect($link, $msg);
	}

	public function publish()
	{
		$input = $this->input;
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_PUBLISH'));
		}
		$model=$this->getModel('treetomatch');
		if (!$model->publish($cid,1))
		{
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=treetomatch.display');
	}

	public function unpublish()
	{
		$input = $this->input;
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_UNPUBLISH'));
		}
		$model=$this->getModel('treetomatch');
		if (!$model->publish($cid,0))
		{
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=treetomatch.display');
	}
}
?>