<?php
/**
* @copyright	Copyright (C) 2006-2015 joomleague.at. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Joomleague Component Controller
 *
 * @package	JoomLeague
 * @since	0.1
 */
class JoomleagueControllerProjectteam extends JoomleagueController
{
	protected $view_list = 'projectteams';
	
	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('add', 'edit');
		$this->registerTask('apply', 'save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$document	= JFactory::getDocument();
	 	$model		= $this->getModel('projectteams');
		$viewType	= $document->getType();
		$view		= $this->getView('projectteams', $viewType);
		$view->setModel($model, true);  // true is for the default model;

		$app = JFactory::getApplication();
		$projectws = $this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project', 0));
		$view->setModel($projectws);

		parent::display();
	}

	public function edit()
	{
		$document	= JFactory::getDocument();
		$viewType	= $document->getType();
		$view		= $this->getView  ('projectteam', $viewType);

		$app	= JFactory::getApplication();
		$projectws = $this->getModel ('project');
		$projectws->setId($app->getUserState('com_joomleagueproject', 0));
		$view->setModel($projectws);

		$input = $this->input;
		$input->set('view', 'projectteam');
		$input->set('layout', 'form');
		$input->set('hidemainmenu', $input->get('hidemainmenu',0));

		$model 	= $this->getModel();
		$user	= JFactory::getUser();

		// Error if checkedout by another administrator
		if ($model->isCheckedOut($user->get('id'))) {
			$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectteam.display', JText::_('EDITED BY ANOTHER ADMIN'));
		}

		$model->checkout();

		parent::display();
	}

	public function storechangeteams()
	{
		$input = $this->input;
	  	$oldteamids	= $input->post->get('oldptid', array(), 'array');
		$newteamids	= $input->post->get('newptid', array(), 'array');
	
		if ( $oldteamids )
	    {
			$model = $this->getModel('projectteams');
	    	$app = JFactory::getApplication();
	    	if(!$model->changeTeamId($oldteamids, $newteamids, $app)) {
	    		$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_ERROR_SAVE') . $model->getError(); 
	    	}
	    }	
	    $link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectteam.display';
	  	$this->setRedirect($link, $msg);  
	}
  
  	public function changeteams()
	{
		$app	= JFactory::getApplication();
		$document	= JFactory::getDocument();
		$model		= $this->getModel ('projectteams');
		$viewType	= $document->getType();
		$view		= $this->getView  ('projectteams', $viewType);
		$view->setModel($model, true);  // true is for the default model;

		$projectws = $this->getModel ('project');
		$projectws->setId($app->getUserState($this->option.'project', 0));
		$view->setModel($projectws);

		$input = $this->input;
		$input->set('hidemainmenu', $input->get('hidemainmenu',0));
		$input->set('layout', 'changeteams' );
		$input->set('view', 'projectteams');
		$input->set('edit', true);

		// Checkout the project
		//	$model = $this->getModel('projectteam');

		parent::display();
	}
  
  function editlist()
	{
		$document	= JFactory::getDocument();
		$model		= $this->getModel ('projectteams');
		$viewType	= $document->getType();
		$view		= $this->getView  ('projectteams', $viewType);
		$view->setModel($model, true);  // true is for the default model;

		$app = JFactory::getApplication();
		$projectws = $this->getModel ('project');
		$projectws->setId($app->getUserState($this->option.'project', 0));
		$view->setModel($projectws);

		$input = $this->input;
		$input->set('hidemainmenu', $input->get('hidemainmenu',0));
		$input->set('layout', 'editlist');
		$input->set('view', 'projectteams');
		$input->set('edit', true);

		// Checkout the project
		//	$model = $this->getModel('projectteam');

		parent::display();
	}

	public function save_teamslist()
	{
		$input = $this->input;
		$cid = $input->post->get('cid', array(0), 'array');
		$post = $input->post->getArray();
		$post['id'] = (int) $cid[0];

		$model = $this->getModel('projectteams');
		if ($model->store($post))
		{
			//clear ranking cache
			$cache = JFactory::getCache('joomleague.project'.$post['id']);
			$cache->clean();
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_SAVED');
		}
		else
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_ERROR_SAVE') . $model->getError();
		}

		$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectteam.display';
		$this->setRedirect($link, $msg);
	}

	public function save()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$app	= JFactory::getApplication();
 		$project_id = $app->getUserState($this->option.'project');
		
		$input = $this->input;
		$cid = $input->post->get('cid', array(0), 'array');
		// TODO: Try to combine the raw and filterinput commands on notes
		//       For the moment this is needed to prevent that html formatting (e.g. <p>) is removed.
		$notes = $input->post->get('notes', 'none', 'raw');
		$filter = JFilterInput::getInstance(null, null, 1, 1, 0);
		$post = $input->post->getArray();
		$post['id'] = (int) $cid[0];
		$post['notes'] = $filter->clean($notes, 'html');
 		
		$model = $this->getModel();
		if (isset($post['add_trainingData']))
		{
			if ($model->addNewTrainingData($post['id'],(int) $post['project_id']))
			{
				$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_TRAINING');
			}
			else
			{
				$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_ERROR_TRAINING').$model->getError();
			}
			//echo $msg;
		}

		if (isset($post['tdCount'])) // Existing Team Trainingdata
		{
			if ($model->saveTrainingData($post))
			{
				$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_TRAINING_SAVED');
			}
			else
			{
				$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_TRAINING_ERROR_SAVE').$model->getError();
			}

			if ($model->checkAndDeleteTrainingData($post))
			{
				$msg .= ' - '.JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_TRAINING_DELETED');
			}
			else
			{
				$msg = ' - '.JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_TRAINING_ERROR_DELETED').$model->getError();
			}
			$msg .= ' - ';
		}

		if ($model->store($post))
		{
			//clear ranking cache
			$cache = JFactory::getCache('joomleague.project'.$project_id);
			$cache->clean();
			
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_TEAM_SAVED');
		}
		else
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_TEAM_ERROR_SAVE').$model->getError();
		}

		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ($this->getTask()=='save')
		{
			$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectteam.display';
		}
		else
		{
			$link = 'index.php?option='.$this->option.'&task=projectteam.edit&cid[]=' . $post['id'];
		}
		//echo $msg;
		$this->setRedirect($link,$msg);
	}

	// save the checked rows inside the project teams list
	public function saveshort()
	{
		$app	= JFactory::getApplication();
 		$project_id = $app->getUserState($this->option.'project');
		
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		$post = $input->post->getArray();
		
		$model = $this->getModel('projectteams');
		if ($model->storeshort($cid, $post))
		{
			//clear ranking cache
			$cache = JFactory::getCache('joomleague.project'.$project_id);
			$cache->clean();
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_UPDATED');
		}
		else
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_CTRL_ERROR_UPDATED') . $model->getError();
		}

		$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectteam.display';
		$this->setRedirect($link, $msg);
	}

	public function remove()
	{
		$app = JFactory::getApplication();
		$project_id = $app->getUserState($this->option.'project',0);
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500, JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_DELETE'));
		}
		
		// Access checks.
		$user = JFactory::getUser();
		foreach ($cid as $i => $id)
		{
			if (!$user->authorise('core.admin', 'com_joomleague') ||
				!$user->authorise('core.admin', 'com_joomleague.project.'.(int) $project_id) || 
				!$user->authorise('core.delete', 'com_joomleague.project_team.'.(int) $id))
			{
				// Prune items that you can't delete.
				unset($cid[$i]);
				JError::raiseNotice(403, JText::_('JERROR_CORE_DELETE_NOT_PERMITTED'));
			}
		}
		
		// TODO: should we remove an entry from team here?
		$model = $this->getModel('team');
		if (!$model->delete($cid))
		{
			echo "<script> alert('" . $model->getError(true) . "'); window.history.go(-1); </script>\n";
		}

		$this->setRedirect('index.php?option='.$this->option.'&view=teams&task=projectteam.display');
	}

	// TODO: is this function needed?
	public function publish()
	{
		$this->setRedirect('index.php?option='.$this->option.'&view=teams&task=projectteam.display');
	}

	// TODO: is this function needed?
	public function unpublish()
	{
		$this->setRedirect('index.php?option='.$this->option.'&view=teams&task=projectteam.display');
	}

	public function cancel()
	{
		// Checkin the project
		$model = $this->getModel();
		$model->checkin();
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectteam.display');
	}

	/**
	 * copy team to another project
	 */
	public function copy()
	{
		$input = $this->input;
		$dest = $input->get('dest');
		$ptids = $input->post->get('ptids', array(), 'array');
		
		// check if this is the final step
		if (!$dest) 
		{
			$input->set('view', 'projectteams');
			$input->set('layout', 'copy');
			
			return parent::display();
		}
		
		$msg  = '';
		$type = 'message';
		
		$model = $this->getModel('projectteams');
		if (!$model->copy($dest, $ptids))
		{
			$msg = $model->getError();
			$type = 'error';
		}
		else
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_PROJECTTEAMS_COPY_SUCCESS');	
		}
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=projectteam.display', $msg, $type);
		$this->redirect();
	}
	
	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'Projectteam', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>
