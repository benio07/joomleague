<?php
/**
* @copyright	Copyright (C) 2006-2014 joomleague.at. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');

/**
 * Joomteam Component Controller
 *
 * @package	JoomLeague
 * @since	0.1
 */
class JoomleagueControllerTeam extends JoomleagueController
{
	protected $view_list = 'teams';
	
	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('add','display');
		$this->registerTask('edit','display');
		$this->registerTask('apply','save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$input = $this->input;
		switch($this->getTask())
		{
			case 'add'	:
			{
				$input->set('hidemainmenu',0);
				$input->set('layout','form');
				$input->set('view','team');
				$input->set('edit',false);

				// Checkout the project
				$model = $this->getModel();
				$model->checkout();
			} break;

			case 'edit'	:
			{
				$input->set('hidemainmenu',0);
				$input->set('layout','form');
				$input->set('view','team');
				$input->set('edit',true);

				// Checkout the project
				$model = $this->getModel();
				$model->checkout();
			} break;

			/*
			case 'copy'	:
			{
				$cid = $input->post->get('cid',array(0),'array');
				$copyID = (int)$cid[0];
				$input->set('hidemainmenu',1);
				$input->set('layout','form');
				$input->set('view','project');
				$input->set('edit',true);
				$input->set('copy',true);
			} break;
			*/
		}
		parent::display();
	}

	public function save()
	{
		// Check for request forgeries
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(0), 'array');
		// TODO: Try to combine the raw and filterinput commands on notes
		//       For the moment this is needed to prevent that html formatting (e.g. <p>) is removed.
		$notes = $input->post->get('notes', 'none', 'raw');
		$filter = JFilterInput::getInstance(null, null, 1, 1, 0);
		$post = $input->post->getArray();
		$post['id'] = (int) $cid[0];
		$post['notes'] = $filter->clean($notes, 'html');

		$model = $this->getModel();
		if ($model->store($post))
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_TEAM_CTRL_SAVED');
		}
		else
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_TEAM_CTRL_ERROR_SAVE').$model->getError();
		}

		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ($this->getTask() == 'save')
		{
			$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=team.display';
		}
		else
		{
			$link = 'index.php?option='.$this->option.'&task=team.edit&cid[]='.$post['id'];
		}
		$this->setRedirect($link, $msg);
	}

	public function copysave()
	{
		$model = $this->getModel();
		if ($model->copyTeams()) //copy team data
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_TEAM_CTRL_COPY_TEAM');
		}
		else
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_TEAM_CTRL_ERROR_COPY_TEAM').$model->getError();
		}
		//echo $msg;
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=team.display',$msg);
	}

	public function remove()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->post->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_DELETE'));
		}

		// Access checks.
		$user = JFactory::getUser();
		foreach ($cid as $i => $id)
		{
			if (!$user->authorise('core.admin', 'com_joomleague') ||
				!$user->authorise('core.delete', 'com_joomleague.team.'.(int) $id))
			{
				// Prune items that you can't delete.
				unset($cid[$i]);
				JError::raiseNotice(403, JText::_('JERROR_CORE_DELETE_NOT_PERMITTED'));
			}
		}
		$model = $this->getModel();
		if(!$model->delete($cid))
		{
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}

		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=team.display');
	}


	public function cancel()
	{
		// Checkin the project
		$model = $this->getModel();
		$model->checkin();
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=team.display');
	}

	public function import()
	{
		$input = $this->input;
		$input->set('view','import');
		$input->set('table','team');
		parent::display();
	}

	public function export()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$cid = $input->get('cid', array(), 'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_EXPORT'));
		}
		$model = $this->getModel();
		$model->export($cid, "team", "Team");
	}

	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'Team', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}	
}
?>