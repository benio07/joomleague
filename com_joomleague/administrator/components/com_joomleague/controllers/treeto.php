<?php
/**
* @copyright	Copyright (C) 2006-2015 joomleague.at. All rights reserved.
* @license		GNU/GPL,see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License,and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Joomleague Component Treeto Controller
 *
 * @package	JoomLeague
 * @since	0.1
 */
class JoomleagueControllerTreeto extends JoomleagueController
{
	protected $view_list = 'treetos';
	
	public function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask('add','display');
		$this->registerTask('edit','display');
		$this->registerTask('apply','save');
	}

	public function display($cachable = false, $urlparams = false)
	{
		$document = JFactory::getDocument();
		$model=$this->getModel('treetos');
		$viewType=$document->getType();
		$view=$this->getView('treetos',$viewType);
		$view->setModel($model,true);  // true is for the default model;

		$app = JFactory::getApplication();
		$projectws=$this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project',0));
		$view->setModel($projectws);

		$input = $this->input;
		switch($this->getTask())
		{
			case 'add':
			{
				$input->set('hidemainmenu',0);
				$input->set('layout','form');
				$input->set('view','treeto');
				$input->set('edit',false);

				$model=$this->getModel();
				//$model->checkout();
				break;
			} 

			case 'edit':
			{
				$input->set('hidemainmenu',0);
				$input->set('layout','form');
				$input->set('view','treeto');
				$input->set('edit',true);

				$model=$this->getModel();
				//$model->checkout();
				break;
			}
		}
		parent::display();
	}

	// save the checked rows inside the treetos list (save division assignment)
	public function saveshort()
	{
		$app	= JFactory::getApplication();
 		$project_id = $app->getUserState($this->option.'project');
		
 		$input = $this->input;
		$post = $input->post->getArray();
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);
		
		$model = $this->getModel('treetos');
		if ($model->storeshort($cid, $post))
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_TREETO_CTRL_SAVED');
		}
		else
		{
			$msg = JText::_('COM_JOOMLEAGUE_ADMIN_TREETO_CTRL_ERROR_SAVED') . $model->getError();
		}

		$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=treeto.display';
		$this->setRedirect($link, $msg);
	}

	public function genNode()
	{
		$input = $this->input;
		$post = $input->post->getArray();
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);

		$model=$this->getModel();

		$document = JFactory::getDocument();
		$viewType=$document->getType();
		$view=$this->getView('treeto',$viewType);
		$view->setModel($model,true);	// true is for the default model;

		$app = JFactory::getApplication();
		$projectws=$this->getModel('project');
		$projectws->setId($app->getUserState($this->option.'project',0));
		$view->setModel($projectws);

		$input->set('hidemainmenu',0);
		$input->set('layout','gennode');
		$input->set('view','treeto');
		$input->set('edit',true);

		// Checkout the project
		//$model=$this->getModel();
		$model->checkout();
		parent::display();
	}

	public function generatenode()
	{
		JSession::checkToken() or die(JText::_('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN'));
		$post=$this->input->post->getArray();
		$model=$this->getModel();
		$app = JFactory::getApplication();
		$project_id=$app->getUserState($this->option.'project');
		if ($model->setGenerateNode() )
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETO_CTRL_GENERATE_NODE');
			$link = 'index.php?option='.$this->option.'&view=treetonodes&task=treetonode.display&tid[]='.$post['id'];
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETO_CTRL_ERROR_GENERATE_NODE').$model->getError();
			$link = 'index.php?option='.$this->option.'&view='.$this->view_list.'&task=treeto.display';
		}
		$this->setRedirect( $link, $msg );
	}

	public function save()
	{
		JSession::checkToken() or die('COM_JOOMLEAGUE_GLOBAL_INVALID_TOKEN');
		$input = $this->input;
		$post = $input->post->getArray();
		$cid = $input->post->get('cid',array(0),'array');
		$post['id'] = (int) $cid[0];
		$msg='';

		$model=$this->getModel('treeto');
		if ($model->store($post))
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETO_CTRL_SAVED');
		}
		else
		{
			$msg=JText::_('COM_JOOMLEAGUE_ADMIN_TREETO_CTRL_ERROR_SAVED').$model->getError();
		}
		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();
		if ($this->getTask()=='save')
		{
			$link='index.php?option='.$this->option.'&view='.$this->view_list.'&task=treeto.display';
		}
		else
		{
			$link='index.php?option='.$this->option.'&task=treeto.edit&cid[]='.$post['id'];
		}
		$this->setRedirect($link,$msg);
	}

	public function remove()
	{
		$input = $this->input;
		$cid = $input->post->get('cid',array(),'array');
		JArrayHelper::toInteger($cid);
		if (count($cid) < 1)
		{
			JError::raiseError(500,JText::_('COM_JOOMLEAGUE_GLOBAL_SELECT_TO_DELETE'));
		}
		$model=$this->getModel();
		if (!$model->delete($cid))
		{
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=treeto.display');
	}

	public function cancel()
	{
		// Checkin the project
		#$model=$this->getModel('treeto');
		#$model->checkin();
		$this->setRedirect('index.php?option='.$this->option.'&view='.$this->view_list.'&task=treeto.display');
	}

	/**
	 * Proxy for getModel
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 *
	 * @return	object	The model.
	 * @since	1.6
	 */
	public function getModel($name = 'Treeto', $prefix = 'JoomleagueModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>
