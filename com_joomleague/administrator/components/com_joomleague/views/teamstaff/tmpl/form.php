<?php defined('_JEXEC') or die('Restricted access');?>
<!-- import the functions to move the events between selection lists	-->
<div id="j-main-container" class="span10">
<form action="index.php" method="post" id="adminForm">
	<div class="col50" id="teamstaff">
		<?php
$selector = 'teamstaff';
echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'details')); 
	
echo JHtml::_('bootstrap.addTab', $selector, 'details', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
echo $this->loadTemplate('details');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'picture', JText::_('COM_JOOMLEAGUE_TABS_PICTURE'));
echo $this->loadTemplate('picture');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'description', JText::_('COM_JOOMLEAGUE_TABS_DESCRIPTION'));
echo $this->loadTemplate('description');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'extended', JText::_('COM_JOOMLEAGUE_TABS_EXTENDED'));
echo $this->loadTemplate('extended');
echo JHtml::_('bootstrap.endTab');

if(	JFactory::getUser()->authorise('core.admin', 'com_joomleague') ||
	JFactory::getUser()->authorise('core.admin', 'com_joomleague.team_staff')) {
	echo JHtml::_('bootstrap.addTab', $selector, 'permissions', JText::_('JCONFIG_PERMISSIONS_LABEL'));
	echo $this->loadTemplate('permissions');
	echo JHtml::_('bootstrap.endTab');
}

echo JHtml::_('bootstrap.endTabSet');
	?>	
		<input type="hidden" name="eventschanges_check"	value="0" id="eventschanges_check" />
		<input type="hidden" name="option"				value="com_joomleague" />
		<input type="hidden" name="team"				value="<?php echo $this->teamws->id; ?>" />
		<input type="hidden" name="cid[]"				value="<?php echo $this->project_teamstaff->id; ?>" />
		<input type="hidden" name="task"				value="" />
	</div>
	<?php echo JHtml::_( 'form.token' ); ?>
</form>
</div>