<?php 

defined('_JEXEC') or die('Restricted access');
?>
<fieldset class="form-horizontal">
	<legend><?php echo JText::_('COM_JOOMLEAGUE_ADMIN_LEAGUE_LEGEND'); ?></legend>
	<?php 
		echo $this->form->renderField('name');
		echo $this->form->renderField('middle_name');
		echo $this->form->renderField('short_name');
		echo $this->form->renderField('alias');
	?>
	<div class="control-group">
		<div class="control-label"><?php echo $this->form->getLabel('country'); ?></div>
		<div class="controls"><?php echo $this->form->getInput('country'); ?>&nbsp;<?php echo Countries::getCountryFlag($this->form->getValue('country')); ?>&nbsp;(<?php echo $this->form->getValue('country'); ?>)</div>
	</div>
</fieldset>