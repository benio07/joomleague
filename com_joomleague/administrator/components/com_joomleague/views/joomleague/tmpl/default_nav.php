<?php 
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filesystem.file');

$option = 'com_joomleague';
?>
<form action="index.php?option=com_joomleague" method="post" id="adminForm1">
<div id="sidebar-container" class="span2">
	<div id="sidebar">
		<div class="sidebar-nav">
			<a href='#' style='text-decoration: none; color: green; padding: 2px;'>v<?php echo $this->version; ?></a>
			<?php
			$selector = "joomleagueaccordionadminmenue";
			echo JHtml::_('bootstrap.startAccordion', $selector, array('active'=>$this->active));
			?>
			<div class="filter-select">
				<?php 
				echo $this->lists['sportstypes']; 
				if ($this->sports_type_id) {
					echo $this->lists['seasons'].'<br />';
					echo $this->lists['projects'].'<br />';
				}
				// Project objects
				if ($this->project->id && $this->sports_type_id)
				{
					echo $this->lists['projectteams'];'<br />';
					echo $this->lists['projectrounds'];
				}
				?>
			</div>
			<?php
				$text =  JText::_('COM_JOOMLEAGUE_DATABASE');
				$n		= 0;
				$tabs	=& $this->tabs;
				$link	=& $this->link;
				$label	=& $this->label;
				$limage	=& $this->limage;
				$href	= '';
				$title	= '';
				$image	= '';
				$text	= '';
				foreach ($tabs as $tab)
				{
					$title=$tab->title;
					echo JHtml::_('bootstrap.addSlide', $selector, $title, $tab->name);
					?>
					<ul id="submenu" class="nav nav-list">
						<?php
							for ($i=0; $i<count($link[$n]); $i++)
							{
								$href	= $link[$n][$i];
								$title	= $label[$n][$i];
								$image	= $limage[$n][$i];
								$text	= $label[$n][$i];
								$allowed= true;
								$data 	= JUri::getInstance($href)->getQuery(true);
								$jinput = new JInput($data);
								$task 	= $jinput->getCmd('task');
								if($task != '' && $option == 'com_joomleague')  {
									if (!JFactory::getUser()->authorise($task, 'com_joomleague')) {
										//display the task which is not handled by the access.xml
										//return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR').' Task: '  .$task);
										$allowed = false;
									}
								}
								if($allowed) {
									echo '<li><a href="'.$href.'" title="'.JText::_('JGLOBAL_AUTH_ACCESS_GRANTED').'">'.$image.' '.$text.'</a></li>';
								} else {
									echo '<li><span title="'.JText::_('JGLOBAL_AUTH_ACCESS_DENIED').'">'.$image.' '.$text.'</span></li>';
								}
							}
							?>
					</ul>
					<?php
					echo JHtml::_('bootstrap.endSlide');
					$n++;
				}
				echo JHtml::_('bootstrap.endAccordion');
				//Extension
				$extensions=JoomleagueHelper::getExtensions(1);
				foreach ($extensions as $e => $extension) {
					$JLGPATH_EXTENSION= JPATH_COMPONENT_SITE.'/extensions/'.$extension;
					$menufile = $JLGPATH_EXTENSION.'/admin/views/joomleague/tmpl/default_'.$extension.'.php';
					if(JFile::exists($menufile )) {
						echo $this->loadTemplate($extension);
					} else {
					}
				}
				?>
			<div style="text-align: center;"><br />
			<?php
			echo JHtml::image('administrator/components/com_joomleague/assets/images/jl.png',JText::_('JoomLeague'),array("title" => JText::_('JoomLeague')));
			?>
			</div>
		</div>
	</div>
</div>
<input type="hidden" name="option" value="<?php echo $option; ?>" />
<input type="hidden" name="act" value="" id="jl_short_act" />
<input type="hidden" name="task" value="joomleague.selectws" />
<?php echo JHtml::_('form.token')."\n"; ?>
</form>

		