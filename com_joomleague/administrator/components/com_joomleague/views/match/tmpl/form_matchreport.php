<?php 
defined('_JEXEC') or die('Restricted access');
?>		
<fieldset class="form-horizontal">
	<legend><?php echo JText::_('COM_JOOMLEAGUE_ADMIN_MATCH_F_MR'); ?></legend>
	<?php 
		echo $this->form->renderField('show_report');
		echo $this->form->renderField('summary');
	?>
</fieldset>		