<?php defined('_JEXEC') or die('Restricted access');?>
<div id="j-main-container" class="span10">
<form action="index.php" method="post" id="adminForm">
	<div class="col50" id="project">
	<?php
	$selector = 'project';
	echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'details')); 
	
	echo JHtml::_('bootstrap.addTab', $selector, 'details', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
	echo $this->loadTemplate('details');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'date', JText::_('COM_JOOMLEAGUE_TABS_DATE'));
	echo $this->loadTemplate('date');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'matchday', JText::_('COM_JOOMLEAGUE_TABS_MATCHDAY'));
	echo $this->loadTemplate('matchday');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'competition', JText::_('COM_JOOMLEAGUE_TABS_COMPETITION'));
	echo $this->loadTemplate('competition');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'favorite', JText::_('COM_JOOMLEAGUE_TABS_FAVORITE'));
	echo $this->loadTemplate('favorite');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'picture', JText::_('COM_JOOMLEAGUE_TABS_PICTURE'));
	echo $this->loadTemplate('picture');
	echo JHtml::_('bootstrap.endTab');
	
	echo JHtml::_('bootstrap.addTab', $selector, 'extended', JText::_('COM_JOOMLEAGUE_TABS_EXTENDED'));
	echo $this->loadTemplate('extended');
	echo JHtml::_('bootstrap.endTab');
	
	if(	JFactory::getUser()->authorise('core.admin', 'com_joomleague') || 
		JFactory::getUser()->authorise('core.admin', 'com_joomleague.project')) {
		echo JHtml::_('bootstrap.addTab',$selector, 'permissions', JText::_('JCONFIG_PERMISSIONS_LABEL'));
		echo $this->loadTemplate('permissions');
		echo JHtml::_('bootstrap.endTab');
	}
	echo JHtml::_('bootstrap.endTabSet'); 
	?>
	<div class="clr"></div>
	<input type="hidden" name="option" value="com_joomleague" /> 
	<input type="hidden" name="task" value="" /> 
	<input type="hidden"name="oldseason" value="<?php echo $this->form->getValue('season_id'); ?>" />
	<input type="hidden" name="oldleague" value="<?php echo $this->form->getValue('league_id'); ?>" /> 
	<input type="hidden" name="cid[]" value="<?php echo $this->form->getValue('id'); ?>" />
	<?php echo JHtml::_('form.token')."\n"; ?>
	</div>
</form>
</div>