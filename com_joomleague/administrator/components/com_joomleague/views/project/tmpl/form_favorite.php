<?php 

defined('_JEXEC')or die('Restricted access');
?>
		
<fieldset class="form-horizontal">
	<legend><?php echo JText::_('COM_JOOMLEAGUE_ADMIN_PROJECT_FAV_TEAM'); ?></legend>
	<?php foreach($this->form->getFieldset('favorite') as $field): ?>
		<div class="control-group">	
			<div class="control-label"><?php echo $field->label; ?></div>
			<div class="controls"><?php echo $field->input; ?></div>
		</div>
	<?php endforeach; ?>
</fieldset>		
