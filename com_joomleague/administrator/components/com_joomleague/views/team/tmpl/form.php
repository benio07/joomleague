<?php defined('_JEXEC') or die('Restricted access'); ?>
<div id="j-main-container" class="span10">
<form action="index.php" method="post" id="adminForm">
<div class="col50" id="team">

<?php
$selector = 'team';
echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'details')); 
	
echo JHtml::_('bootstrap.addTab', $selector, 'details', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
echo $this->loadTemplate('details');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'picture', JText::_('COM_JOOMLEAGUE_TABS_PICTURE'));
echo $this->loadTemplate('picture');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'description', JText::_('COM_JOOMLEAGUE_TABS_DESCRIPTION'));
echo $this->loadTemplate('description');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'extended', JText::_('COM_JOOMLEAGUE_TABS_EXTENDED'));
echo $this->loadTemplate('extended');
echo JHtml::_('bootstrap.endTab');

if(	JFactory::getUser()->authorise('core.admin', 'com_joomleague') ||
	JFactory::getUser()->authorise('core.admin', 'com_joomleague.team')) {

	echo JHtml::_('bootstrap.addTab', $selector, 'permissions', JText::_('JCONFIG_PERMISSIONS_LABEL'));
	echo $this->loadTemplate('permissions');
	echo JHtml::_('bootstrap.endTab');
}

echo JHtml::_('bootstrap.endTabSet');
?>
<div class="clr"></div>

<input type="hidden" name="option" value="com_joomleague" />
<input type="hidden" name="cid[]" value="<?php echo $this->form->getValue('id'); ?>" />
<input type="hidden" name="task" value="" />
</div>
<?php echo JHtml::_( 'form.token' ); ?>
</form>
</div>