<?php 
defined('_JEXEC') or die('Restricted access');
?>

<fieldset class="form-vertical">
	<legend><?php echo JText::_('COM_JOOMLEAGUE_ADMIN_TEAM_DESCRIPTION' );?></legend>
	
	<?php foreach($this->form->getFieldset('description') as $field): ?>
		<div class="control-group">	
			<div class="control-label"><?php echo $field->label; ?></div>
			<div class="controls"><?php echo $field->input; ?></div>
		</div>
	<?php endforeach; ?>
</fieldset>
