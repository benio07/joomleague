<?php defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
?>
<div id="j-main-container" class="span10">
<form action="index.php" method="post" id="adminForm">
	<div class="col50" id="treeto">
<?php
$selector = 'treeto';
echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'details')); 

echo JHtml::_('bootstrap.addTab', $selector, 'details', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
echo $this->loadTemplate('details');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.endTabSet');
?>
	<div class="clr"></div>
	<input type="hidden" name="option"	value="com_joomleague" />
	<input type="hidden" name="cid[]" value="<?php echo $this->treeto->id; ?>" />
	<input type="hidden" name="task"	value="" />
	<?php echo JHtml::_('form.token')."\n"; ?>
	</div>
</form>
</div>