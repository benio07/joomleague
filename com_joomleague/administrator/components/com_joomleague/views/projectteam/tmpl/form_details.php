<?php 

defined('_JEXEC')or die('Restricted access');
?>
		<fieldset class="form-horizontal">
			<legend></legend>
			<?php 
				echo $this->form->renderField('admin');
			?>
		
			<?php 
			if ($this->projectws->project_type == 'DIVISIONS_LEAGUE') :
			?>
			<div class="control-group">
				<div class="control-label"><?php echo JText::_('COM_JOOMLEAGUE_ADMIN_P_TEAM_DIV');	?></div>
				<div class="controls">
					<?php 
						$inputappend='';
						if ($this->project_team->division_id == 0)
						{
									$inputappend=' style="background-color:#bbffff"';
						}
						echo JHtml::_(	'select.genericlist',
										$this->lists['divisions'],
										'division_id',
										$inputappend.'class="inputbox" size="1"',
										'value','text', $this->project_team->division_id);
						?>
				</div>
			</div>
			<?php endif;?>
			
			<?php 
				echo $this->form->renderField('standard_playground');
				echo $this->form->renderField('is_in_score');
			?>
			</fieldset>				
				
			<fieldset class="form-horizontal">
			<?php 
				echo $this->form->renderField('start_points');
				echo $this->form->renderField('reason');
			?>
			</fieldset>
		
			<fieldset class="form-horizontal">
			<?php 
				echo $this->form->renderField('use_finally');
				echo $this->form->renderField('matches_finally');
				echo $this->form->renderField('points_finally');
				echo $this->form->renderField('neg_points_finally');
				echo $this->form->renderField('won_finally');
				echo $this->form->renderField('draws_finally');
				echo $this->form->renderField('lost_finally');
				echo $this->form->renderField('homegoals_finally');
				echo $this->form->renderField('guestgoals_finally');
				echo $this->form->renderField('diffgoals_finally');
			?>
			</fieldset>
