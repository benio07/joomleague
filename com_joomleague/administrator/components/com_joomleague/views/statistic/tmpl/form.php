<?php defined('_JEXEC') or die('Restricted access');?>
<div id="j-main-container" class="span10">
<form action="index.php" method="post" id="adminForm" class="form-validate">
	<div class="col50" id="statistic">
		<?php
$selector = 'statistic';
echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'details')); 
	
echo JHtml::_('bootstrap.addTab', $selector, 'details', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
echo $this->loadTemplate('details');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'picture', JText::_('COM_JOOMLEAGUE_TABS_PICTURE'));
echo $this->loadTemplate('picture');
echo JHtml::_('bootstrap.endTab');

if ($this->edit):
echo JHtml::_('bootstrap.addTab', $selector, 'param', JText::_('COM_JOOMLEAGUE_TABS_PARAMETERS'));
echo $this->loadTemplate('param');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'gparam', JText::_('COM_JOOMLEAGUE_TABS_GENERAL_PARAMETERS'));
echo $this->loadTemplate('gparam');		
echo JHtml::_('bootstrap.endTab');
endif;		

echo JHtml::_('bootstrap.endTabSet');
		?>	

	</div>

	<div class="clr"></div>
	<?php if ($this->edit): ?>
		<input type="hidden" name="calculated" value="<?php echo $this->calculated; ?>" />
	<?php endif; ?>
	<input type="hidden" name="option" value="com_joomleague" />
	<input type="hidden" name="cid[]" value="<?php echo $this->form->getValue('id'); ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_( 'form.token' ); ?>
</form>
</div>