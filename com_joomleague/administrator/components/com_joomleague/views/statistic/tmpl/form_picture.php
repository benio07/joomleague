<?php 
defined('_JEXEC') or die('Restricted access');
?>

<fieldset class="form-horizontal">
	<legend><?php echo JText::_( 'COM_JOOMLEAGUE_ADMIN_STAT_PIC' );?></legend>
	<?php 
		echo $this->form->renderField('icon');
	?>
</fieldset>
