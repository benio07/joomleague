<?php defined('_JEXEC') or die('Restricted access');?>
<div id="j-main-container" class="span10">
<form action="index.php" method="post" id="adminForm">
	<div class="col50" id="round">
<?php
$selector = 'round';
echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'details')); 
	
echo JHtml::_('bootstrap.addTab', $selector, 'details', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
echo $this->loadTemplate('details');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.endTabSet');
?>		
	</div>
	<div class="clr"></div>

	<input type="hidden" name="option"		value="com_joomleague" />
	<input type="hidden" name="project_id"	value="<?php echo $this->projectws->id; ?>" />
	<input type="hidden" name="id"			value="<?php echo $this->matchday->id; ?>" />
	<input type="hidden" name="task"		value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
</div>