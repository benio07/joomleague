<?php defined('_JEXEC') or die('Restricted access');
$uri = JUri::getInstance();
?>
<!-- import the functions to move the events between selection lists  -->
<?php
// TODO: I think the script does not need to be loaded here, but check when position editing is working again if that is true...  
//$version = urlencode(JoomleagueHelper::getVersion());
//echo JHtml::script('administrator/components/com_joomleague/assets/js/eventsediting.js?v='.$version);
?>
<div id="j-main-container" class="span10">
<form action="index.php" method="post" id="adminForm">
	<div class="col50" id="position">
<?php
$selector = 'position';
echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'details')); 
	
echo JHtml::_('bootstrap.addTab', $selector, 'details', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
echo $this->loadTemplate('details');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'events', JText::_('COM_JOOMLEAGUE_TABS_EVENTS'));
echo $this->loadTemplate('events');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'statistics', JText::_('COM_JOOMLEAGUE_TABS_STATISTICS'));
echo $this->loadTemplate('statistics');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.endTabSet');
?>
		<div class="clr"></div>
		<input type="hidden" name="eventschanges_check" id="eventschanges_check" value="0" />
		<input type="hidden" name="statschanges_check" id="statschanges_check" value="0" />
		<input type="hidden" name="option" value="com_joomleague" />
		<input type="hidden" name="cid[]" value="<?php echo $this->position->id; ?>" />
		<input type="hidden" name="task" value="" />
	</div>
	<?php echo JHtml::_('form.token')."\n"; ?>
</form>
</div>