<?php 

defined('_JEXEC') or die('Restricted access');
?>		
<fieldset class="form-horizontal">
	<legend><?php echo JText::_('COM_JOOMLEAGUE_ADMIN_POSITION_DETAILS_LEGEND'); ?></legend>
	<?php 
		echo $this->form->renderField('name');
		echo $this->form->renderField('alias');
		echo $this->form->renderField('sports_type_id');
		echo $this->form->renderField('published');
		echo $this->form->renderField('persontype');
	?>
	<div class="control-group">
		<div class="control-label"><?php echo JText::_('COM_JOOMLEAGUE_ADMIN_POSITION_P_POSITION'); ?></div>
		<div class="controls"><?php echo $this->lists['parents']; ?></div>
	</div>
</fieldset>	