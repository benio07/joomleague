<?php defined('_JEXEC') or die('Restricted access');?>
<div id="j-main-container" class="span10">
<form action="index.php" method="post" id="adminForm">
	<div class="col50" id="projectreferee">
<?php
$selector = 'projectreferee';
echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'details')); 
	
echo JHtml::_('bootstrap.addTab', $selector, 'details', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
echo $this->loadTemplate('details');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'picture', JText::_('COM_JOOMLEAGUE_TABS_PICTURE'));
echo $this->loadTemplate('picture');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'description', JText::_('COM_JOOMLEAGUE_TABS_DESCRIPTION'));
echo $this->loadTemplate('description');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'extended', JText::_('COM_JOOMLEAGUE_TABS_EXTENDED'));
echo $this->loadTemplate('extended');
echo JHtml::_('bootstrap.endTab');

if(	JFactory::getUser()->authorise('core.admin', 'com_joomleague') ||
	JFactory::getUser()->authorise('core.admin', 'com_joomleague.project.'.(int)$this->projectws->id)) {
	echo JHtml::_('bootstrap.addTab', $selector, 'permissions', JText::_('JCONFIG_PERMISSIONS_LABEL'));
	echo $this->loadTemplate('permissions');
	echo JHtml::_('bootstrap.endTab');
}

echo JHtml::_('bootstrap.endTabSet');
?>
		<input type="hidden" name="option" value="com_joomleague" /> 
		<input type="hidden" name="project_id"  value="<?php echo $this->projectws->id; ?>" /> 
		<input type="hidden" name="id"  value="<?php echo $this->projectreferee->id; ?>" /> 
		<input type="hidden" name="task"   value="" />
	</div>
	<?php echo JHtml::_('form.token')."\n"; ?>
</form>
</div>