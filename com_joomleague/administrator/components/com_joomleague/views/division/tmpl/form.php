<?php defined('_JEXEC') or die('Restricted access');?>
<div id="j-main-container" class="span10">
<form action="index.php" method="post" id="adminForm">
	<div class="col50" id="division">
		<?php
$selector = 'division';
echo JHtml::_('bootstrap.startTabSet', $selector, array('active'=>'details')); 
	
echo JHtml::_('bootstrap.addTab', $selector, 'details', JText::_('COM_JOOMLEAGUE_TABS_DETAILS'));
echo $this->loadTemplate('details');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'picture', JText::_('COM_JOOMLEAGUE_TABS_PICTURE'));
echo $this->loadTemplate('picture');
echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.addTab', $selector, 'description', JText::_('COM_JOOMLEAGUE_TABS_DESCRIPTION'));
echo $this->loadTemplate('description');
echo JHtml::_('bootstrap.endTab');

//echo JHtml::_('bootstrap.addTab', $selector, 'extended', JText::_('COM_JOOMLEAGUE_TABS_EXTENDED'));
//echo $this->loadTemplate('extended');
//echo JHtml::_('bootstrap.endTab');

echo JHtml::_('bootstrap.endTabSet');
		?>
		<div class="clr"></div>

		<input type="hidden" name="option" value="com_joomleague" /> 
		<input type="hidden" name="cid[]" value="<?php echo $this->division->id; ?>" /> 
		<input type="hidden" name="project_id" value="<?php echo $this->projectws->id; ?>" /> 
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_( 'form.token' ); ?>
	</div>
</form>
</div>