<?php 

defined('_JEXEC') or die('Restricted access');
foreach ($this->extended->getFieldsets() as $fieldset)
{
	?>
	<fieldset class="form-vertical">
	<legend><?php echo JText::_($fieldset->name); ?></legend>
	<?php
	$fields = $this->extended->getFieldset($fieldset->name);
	
	if(!count($fields)) {
		echo JText::_('COM_JOOMLEAGUE_GLOBAL_NO_PARAMS');
	}
	
	foreach ($fields as $field)
	{
		?>
		<div class="control-group">
			<div class="control-label"><?php echo $field->label; ?></div>
			<div class="control-label"><?php echo $field->input; ?></div>
		</div>
		<?php
	}
	?>
	</fieldset>
	<?php
}
