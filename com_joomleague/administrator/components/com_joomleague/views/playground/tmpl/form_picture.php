<?php 
defined('_JEXEC') or die('Restricted access');
?>

<fieldset class="adminform">
	<legend><?php echo JText::_('COM_JOOMLEAGUE_ADMIN_PLAYGROUND_PICTURE' );?></legend>
	<?php foreach($this->form->getFieldset('picture') as $field): ?>
		<div class="control-group">	
			<div class="control-label"><?php echo $field->label; ?></div>
			<div class="controls"><?php echo $field->input; ?></div>
		</div>
	<?php endforeach; ?>
</fieldset>
