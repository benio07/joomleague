<?php
/**
 * @copyright	Copyright (C) 2006-2014 joomleague.at. All rights reserved.
 * @license		GNU/GPL,see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License,and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controlleradmin');

/**
 * Joomleague Common Controller
 *
 * @package	JoomLeague
 * @since	2.5
 */

class JoomleagueController extends JLGControllerAdmin
{

	public function display($cachable = false, $urlparams = false)
	{
		$input = $this->input;
		// display the left menu only if hidemainmenu is not true
		$show_menu=!$input->get('hidemainmenu',false);

		// display left menu
		$viewName = $input->get('view', '');
		$layoutName = $input->get('layout', 'default');
		if($viewName == '' && $layoutName=='default') {
			$input->set('view', 'projects');
			$viewName = "projects";
		}
		if ($viewName != 'about' && $show_menu) {
			$this->ShowMenu();
		} else {
			$pid=$input->get('pid',array(0),'array');
			if($pid[0] > 0)
			{
				$app = JFactory::getApplication();
				$app->setUserState($this->option.'project',$pid[0]);
			}
		}
		$document = JFactory::getDocument();
		$viewType = $document->getType();
		$view = $this->getView($viewName,$viewType);
		$view->setLayout($layoutName);
		$model = $this->getModel($viewName);
		$view->setModel($model, true);
		$view->display();
		parent::display($cachable, $urlparams);
	}

	public function ShowMenu()
	{
		$document	= JFactory::getDocument();
		$viewType	= $document->getType();
		$view		= $this->getView('joomleague',$viewType);
		if ($model = $this->getModel('project'))
		{
			// Push the model into the view (as default)
			$app = JFactory::getApplication();
			$model->setId($app->getUserState($this->option.'project',0));
			$view->setModel($model,true);
		}
		$view->display();
	}

	public function ShowMenuExtension()
	{
		$document = JFactory::getDocument();
		$viewType=$document->getType();
		$view = $this->getView('joomleague',$viewType);
		$view->setLayout('extension');
		$view->display();
	}

	public function selectws()
	{
		$input = $this->input;
		$stid	= $input->get('stid',array(0),'array');
		$pid	= $input->get('pid',array(0),'array');
		$tid	= $input->get('tid',array(0),'array');
		$rid	= $input->get('rid',array(0),'array');
		$sid	= $input->get('seasonnav',array(0),'array');
		$act	= $input->get('act',0);

		$seasonnav = $input->getInt('seasonnav');
		$app = JFactory::getApplication();
		$app->setUserState($this->option.'seasonnav', $seasonnav);

		switch ($act)
		{
			case 'projects':
				if ($app->setUserState($this->option.'project',(int)$pid[0]))
				{
					$app->setUserState($this->option.'project_team_id','0');
					$this->setRedirect('index.php?option=com_joomleague&task=joomleague.workspace&layout=panel&pid[]='.$pid[0],JText::_('COM_JOOMLEAGUE_ADMIN_CTRL_PROJECT_SELECTED'));
				}
				else
				{
					$this->setRedirect('index.php?option=com_joomleague&view=projects&task=project.display');
				}
				break;

			case 'teams':
				$app->setUserState($this->option.'project_team_id',(int)$tid[0]);
				if ((int) $tid[0] != 0)
				{
					$this->setRedirect('index.php?option=com_joomleague&view=teamplayers&task=teamplayer.display',JText::_('COM_JOOMLEAGUE_ADMIN_CTRL_TEAM_SELECTED'));
				}
				else
				{
					$this->setRedirect('index.php?option=com_joomleague&task=joomleague.workspace&layout=panel&pid[]='.$pid[0]);
				}
				break;

			case 'rounds':
				if ((int) $rid[0] != 0)
				{
					$this->setRedirect('index.php?option=com_joomleague&view=matches&task=match.display&rid[]='.$rid[0],JText::_('COM_JOOMLEAGUE_ADMIN_CTRL_ROUND_SELECTED'));
				}
				break;

			case 'seasons':
				$this->setRedirect('index.php?option=com_joomleague&view=projects&task=project.display&sid[]='.$sid[0], JText::_('COM_JOOMLEAGUE_ADMIN_CTRL_SEASON_SELECTED'));
				break;

			default:
				if ($app->setUserState($this->option.'sportstypes',(int)$stid[0]))
				{
					$app->setUserState($this->option.'project','0');
					$this->setRedirect('index.php?option=com_joomleague&task=project.display&view=projects&stid[]='.$stid[0],JText::_('COM_JOOMLEAGUE_ADMIN_CTRL_SPORTSTYPE_SELECTED'));
				}
				else
				{
					$this->setRedirect('index.php?option=com_joomleague&view=sportstypes&task=sportstype.display');
				}
		}

	}
}

/**
 *
 * just to display the cpanel
 * @author And_One
 *
 */
class JoomleagueControllerJoomleague extends JoomleagueController {
}
